---
title: Home
---

<!-- MarkdownTOC -->

- [Terminal](#terminal)
  - [Set ZSH](#set-zsh)
  - [Tmux config](#tmux-config)
- [Sublime Settings](#sublime-settings)

<!-- /MarkdownTOC -->

# Server Commands

```bash
mysqldump -u username -p database_to_backup > backup_name.sql
scp -i /path/to/public/key USER@SERVERIP:/path/on/remote/droplet /path/to/my/files
```

# Terminal

#### Show git bash branch

```bash
# Add git branch if its present to PS1
parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

export PS1='${debian_chroot:+($debian_chroot)}\[\033[0;37m\][\@] \[\033[0;35m\]\w\[\033[01;36m\] $(parse_git_branch)\[\033[00m\]\n\$ '
```

## Set ZSH

1. Install ZSH: `sudo apt-get install zsh`
2. Verify installtion: `zsh --version`
3. Install: sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
4. Set as default shell: `chsh -s $(which zsh)`
5. Log out and in

#### ZSH Theme

1. Get Powerline Font: `sudo apt-get install fonts-powerline`
2. Edit `~/.zshrc`

```bash
ZSH_THEME="agnoster"
USER=``
```

3. Edit `~/.oh-my-zsh/themes/agnoster.zsh-theme`

```bash
# Add
prompt_time() {
  prompt_segment white $CURRENT_FG $(date +%X )
}
# Search build_prompt(), add in prompt_time just before prompt_dir
```

4. Follow instructions in [Agnoster Newline](https://github.com/agnoster/agnoster-zsh-theme/issues/65). Add `\n ➜`

**Source**

- [Oh My ZSH](https://github.com/robbyrussell/oh-my-zsh)

## Tmux config

1. Install `tmux` from software center
2. Create `~/.tmux.conf` file
3. Set tmux theme

```bash
git clone https://github.com/gpakosz/.tmux.git
ln -s -f .tmux/.tmux.conf
cp .tmux/.tmux.conf.local .
```

4. Edit `~/.tmux.conf`

```bash
set -g mouse on
# Set default shell to zsh
set-option -g default-shell /bin/zsh
```

5. Add powerline font separator in `~/.tmux.conf.local`:

```bash
tmux_conf_theme_left_separator_main=''
tmux_conf_theme_left_separator_sub=''
tmux_conf_theme_right_separator_main=''
tmux_conf_theme_right_separator_sub=''
```

6. Run `tmux source-file ~/.tmux.conf` to restart

**Source**:

- [Tmux Theming](https://github.com/gpakosz/.tmux)
- [Tmux KeyBindings](https://lukaszwrobel.pl/blog/tmux-tutorial-split-terminal-windows-easily/)

# Sublime Settings

#### Sublime packages

```
"AdvancedNewFile",
"Babel",
"CSS3",
"DocBlockr",
"Emmet",
"Format JSDoc @params",
"Git blame",
"GitGutter",
"JSCustom",
"JsPrettier",
"Laravel Blade Highlighter",
"MarkdownLivePreview",
"MarkdownTOC",
"Package Control",
"PackageResourceViewer",
"ProjectManager",
"SCSS",
"Seti_UI",
"SideBarEnhancements",
"SublimeERB",
"SublimeLinter",
"SublimeLinter-eslint",
"SublimeLinter-rubocop",
"Theme - Seti Monokai",
"yardoc"
```

#### Sublime key bindings

```json
[
  { "keys": ["ctrl+shift+s"], "command": "save_all" },
  { "keys": ["ctrl+alt+shift+s"], "command": "prompt_save_as" },
  { "keys": ["ctrl+shift+."], "command": "erb" },
  {
    "keys": ["ctrl+t"],
    "command": "show_overlay",
    "args": { "overlay": "goto", "show_files": true }
  },
  {
    "keys": ["/"],
    "command": "close_tag",
    "args": { "insert_slash": true },
    "context": [
      {
        "key": "selector",
        "operator": "equal",
        "operand": "(text.html, text.xml, meta.jsx.js) - string - comment",
        "match_all": true
      },
      {
        "key": "preceding_text",
        "operator": "regex_match",
        "operand": ".*<$",
        "match_all": true
      },
      { "key": "setting.auto_close_tags" }
    ]
  },
  {
    "keys": ["tab"],
    "command": "expand_abbreviation_by_tab",
    "context": [
      {
        "operand": "source.js",
        "operator": "equal",
        "match_all": true,
        "key": "selector"
      },
      { "match_all": true, "key": "selection_empty" },
      {
        "operator": "equal",
        "operand": false,
        "match_all": true,
        "key": "has_next_field"
      },
      {
        "operand": false,
        "operator": "equal",
        "match_all": true,
        "key": "auto_complete_visible"
      },
      { "match_all": true, "key": "is_abbreviation" }
    ]
  },
  { "keys": ["ctrl+\\"], "command": "toggle_side_bar" },
  { "keys": ["ctrl+v"], "command": "paste_and_indent" },
  { "keys": ["super+shift+v"], "command": "paste" },
  {
    "keys": ["ctrl+super+left"],
    "command": "move_to",
    "args": { "to": "bol", "extend": false }
  },
  {
    "keys": ["ctrl+super+right"],
    "command": "move_to",
    "args": { "to": "eol", "extend": false }
  },
  { "keys": ["ctrl+shift+2"], "command": "jsdoc_param" }
]
```

#### Sublime settings

```json
{
  "Seti_tabs_small": true,
  "binary_file_patterns": ["*.svg", "*.min.js", "*composite_product_wizard*"],
  "close_windows_when_empty": false,
  "color_scheme": "Packages/Theme - Seti Monokai/scheme/Seti Monokai.tmTheme",
  "detect_indentation": true,
  "file_exclude_pattners": ["*.map"],
  "folder_exclude_patterns": [
    ".svn",
    ".git",
    ".hg",
    "CVS",
    ".svg",
    "node_modules",
    "i18n"
  ],
  "font_size": 14,
  "ignored_packages": ["Vintage"],
  "save_on_focus_lost": false,
  "tab_size": 2,
  "theme": "Seti_orig.sublime-theme",
  "translate_tabs_to_spaces": true,
  "trim_trailing_white_space_on_save": true,
  "word_wrap": true
}
```

#### MarkdownTOC User Setting

```json
{
  "defaults": {
    "autoanchor": false,
    "autolink": true,
    "bracket": "round",
    "levels": [1, 2, 3],
    "indent": "\t",
    "remove_image": true,
    "link_prefix": "",
    "bullets": ["*"],
    "lowercase": "only_ascii",
    "style": "unordered",
    "uri_encoding": true,
    "markdown_preview": ""
  },
  "id_replacements": [
    {
      "pattern": "[_*]{2}([^\\s])[_*]{2}",
      "replacement": "\\1"
    },
    {
      "pattern": "[_*]([^\\s])[_*]",
      "replacement": "\\1"
    },
    {
      "pattern": "\\s+",
      "replacement": "-"
    },
    {
      "pattern": "&lt;|&gt;|&amp;|&apos;|&quot;|&#60;|&#62;|&#38;|&#39;|&#34;|!|#|$|&|'|\\(|\\)|\\*|\\+|,|/|:|;|=|\\?|@|\\[|\\]|`|\"|\\.|\\\\|<|>|{|}|™|®|©",
      "replacement": ""
    }
  ],
  "logging": false
}
```
