---
title: Binary Algorithm
---
<!-- MarkdownTOC -->

* [Binary](#binary)
	* [ASCII](#ascii)
* [Algorithms](#algorithms)

<!-- /MarkdownTOC -->

# Binary
**Decimal** has 10 digits 0 – 9. Computers only work with **binary**, which has two digits, 0 and 1.

#### Decimal
In decimal, 123 is one hundred and twenty-three. And here's why:

![decimal](./images/decimal.png)

The rightmost digit represents the "ones column", next digit represents the "tens columns".  
So 100 + 20 + 3 = 123
#### Binary
Binary uses columns in the same way, but rather than each column representing a power of 10, it represents power of 2.

Number 0

![num_0](./images/num_0.png)

Number 1

![num_1](./images/num_1.png)

Number 7

![num_7](./images/num_7.png)

Each `0` or `1` is stored in the state of a ~~transistor~~ in a computer, which we can also represented by a light bulb or a switch. A light bulb turned off is like a `0` and a light bulb on is like a `1`.

## ASCII
A scheme that maps letters to numbers (which maps  `65` to *A*, `66` to *B*, and so on)

Numbers can also be used to represent audio, video , images etc – the mapping from numbers to other information is determined by the kind of file (e.g., a file ending in .jpg will be interpreted as an image).

One mapping from numbers to images is known as RGB, because it represents each pixel by the intensity of red, green, and blue in that pixel. These values are combined to produce the color that the pixel is displayed.

# Algorithms
An **algorithm** is a set of instructions to solve a particular problem step by step, taking inputs and producing outputs.
For example, how might we find someone like Mike Smith in the phone book?

- We can **look at each page in order**, looking at page one, then page two, and so on… this algorithm is correct (i.e., we will definitely find Mike Smith if he’s in the phone book), but it’s very slow.
- We could **flip two pages at a time** instead, which would make it almost twice as fast (since we might have to go back one page if we overshoot).
- But usually, when we’re looking for someone in the phone book, we **open to the middle**, taking advantage of the fact that phone book is sorted alphabetically. The middle is the M section, so we can literally tear this problem in half by ripping the phone book in half and keeping only the half that we think Mike Smith is in. If we keep repeating this, keeping only half of the book each time, eventually we’re left with a single page containing Mike Smith’s phone number. For a 1,000-page phone book, this only takes about ten steps!

#### Graph of the 3 methods
![book_problem](./images/book_problem.png)
- The red line is the first algorithm, where the time to find Mike Smith is linearly proportional to the number of pages in the phone book.
- The yellow line is the second algorithm, which, though twice as fast, still increases linearly with the size of the problem.
- The green line, however, is not linear, but is instead logarithmic and increases MUCH more slowly than either of the linear algorithms. With this strategy, even **if the size of the phone book doubled**, it would only **take one more step** to solve the problem - and if the phone book contained four billion pages, we’d still only need 32 steps. (Note that this requires that the phone book be alphabetized - if the names were randomly ordered, we might throw away a half of the problem that actually contained Mike Smith, and then we’d never find him!)

Pseudocode
: Pseudocode is a way of describing an algorithm using English rather than any particular programming language:
![pseudo](./images/pseudo.png)
