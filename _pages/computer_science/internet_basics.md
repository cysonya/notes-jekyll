---
title: Internet Basics
---

<!-- MarkdownTOC -->

* [How the internet works](#how-the-internet-works)
* [DNS](#dns)
* [Browser and the web](#browser-and-the-web)

<!-- /MarkdownTOC -->

# How the internet works
Computers connected to the internet wire can communicate.

Internet Protocol Suite (TCP/IP)
:  TCP/IP created a set of rules that allow computers to send info back and forth.

Bandwidth
:  Bandwidth is the amount of data that can be sent over your internet per second.

IP Address
:  IP address help computers find and communicate with each other. *Ex*, google.com is actually an ip address (193.242.132)

ISP (internet service provider)
:  Home computers connect to an ISP (internet service provider) to get on the internet. When picture or email travels across itnernet, the info is broken into pieces called ~~packets~~, and then are assembled in original order.

Routers
:  Routers direct packets to reach their destinations.  
Anywhere two or more parts of the internet interesect has a router.
![routes](./images/routes.png)

# DNS
DNS
:  DNS is used to translate a domain name into ip addresses. *Ex.* www.google.com is translated to 192.231.452

#### How does it work?
In order to search up example.com
1. You type in www.example.com in your browser  
2. Your operating system asks the ~~resolving name server~~ for www.example.com  
3. Resolving name servers finds the ~~root name server~~  
4. Root name server direct you to ~~com name servers~~ or TLD (top level domain) name servers
5. Who then finds the ~~Authoritative name servers~~  
6. It gives your browser the IP address to go to

`.` (after www) points to the root name server
`.com` points to com name servers or top level domain (TLD) name servers
`example.com` points to Authoritative name servers

# Browser and the web
Cloud Computing
:  Datas such as photos, files, emails etc are stored into "the cloud". All this data is accessible from any internet connected computer in the world

Web Apps
:  Applications that perform tasks inside the web browser. *Ex.* Google Maps

#### Benefits of web apps
- Data can be accessed from anywhere
- You'll always get the latest version of any app
- Works on every device with a web browser. Apps work on both PC and Mac
- It is safe. No need to download on your computer, which means better protection from viruses  
- Does not affect overall performance of your machine

#### XHR (XML Http Request)
When XHR was introduced to Javascript, it enabled individual parts of a web page to be altered without needing to reload the whole page.

AJAX
:  Combination of Javascript, XHR, and CS

Cookies
:  Cookies contain information about your visit that you may want your browser to remember, like preferred language and other settings. The browser stores this data and pulls it out the next time you visit the site.
