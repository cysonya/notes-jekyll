---
title: Forum Pt.2
---
<!-- MarkdownTOC -->

* [Favorite Replies](#favorite-replies)
* [Refactor Queries](#refactor-queries)
    * [Global Scope Eager load](#global-scope-eager-load)
* [Delete Thread](#delete-thread)
    * [Policy guard](#policy-guard)
* [Admin Permission](#admin-permission)
* [Record Activity](#record-activity)
* [View Component](#view-component)
* [Activity Feed](#activity-feed)
* [Delete Activities](#delete-activities)
* [Delete Reply](#delete-reply)

<!-- /MarkdownTOC -->

# Favorite Replies
Create a `Favorite` model so signed in users can favorite replies. Ensure that they can only favorite a reply once!
```bash
php artisan make:model Favorite -mc
```
#### Setup table
```php
<?php
Schema::create('favorites', function (Blueprint $table) {
    $table->increments('id');
    $table->unsignedInteger('user_id');
    $table->unsignedInteger('favorited_id');
    $table->string('favorited_type', 50);
    $table->timestamps();
    // set a unique combination, so user cannot favorite a reply more than once!
    $table->unique(['user_id', 'favorited_id', 'favorited_type']);
});
```
#### Define routes, controller and relationship
```php
<?php
// Routes -> /replies/1/favorites
Route::post('/replies/{reply}/favorites', 'FavoritesController@store');
// FavoritesController
public function store(ForumReply $reply)
{
    return $reply->favorite();
}
// ForumReply
public function favorites()
{
    return $this->morphMany(Favorite::class, 'favorited');
}

public function favorite()
{
    $attributes = ['user_id' => auth()->id()];
    // User can only favorite a reply once
    if(! $this->favorites()->where($attributes)->exists()) {
        return $this->favorites()->create($attributes);
    }
}
```
#### Test case
```php
<?php
function test_guest_can_not_favorite_anything()
{
    $this->withExceptionHandling()
        ->post('replies/1/favorites')
        ->assertRedirect('/login');
}

public function test_authenticated_user_can_favorite_any_reply()
{
    $this->signIn();
    $reply = create('App\ForumReply');

    // If I post to a 'favorite' endpoint
    $this->post('replies/'.$reply->id.'/favorites');
    // It should be recorded in the database
    $this->assertCount(1, $reply->favorites);
}

public function test_authenticated_user_can_only_favorite_reply_once()
{
    // Given a signed in user and a forum reply
    $this->signIn();
    $reply = create('App\ForumReply');

    // If I favorite a reply more than once
    try {
        $this->post('replies/'.$reply->id.'/favorites');
        $this->post('replies/'.$reply->id.'/favorites');
    } catch (\Exception $e) {
        $this->fail('Cannot insert same record set twice');
    }
    // It should be 1 record in database
    $this->assertCount(1, $reply->favorites);
}
```
# Refactor Queries
When performing a `foreach` loop, we have to do a query in each iteration. We should *eager-load* this kind of query, and show it using an attribute, like so:
```php
<?php
// In ForumReply
protected $with = ['owner', 'favorites'];

// Find me the favorite where user_id = signed in user id. If count > 1, return true!
public function isFavorited()
{
    return !! $this->favorites->where('user_id', auth()->id())->count();
}
```
This means we want to eager-load the owner into every single reply query. Now when we fetch a reply, the owner and favorites will be included in the query:
```php
<?php
App\Reply {
    id: 1,
    thread_id: 11,
    ...
    owner: App\User {
        id: 12,
        name: 'John Doe'
        ...
    },
    favorites: App\Favorites {
        all: [...]
    }
}
```
## Global Scope Eager load
We want to eager load the creator when we fetch all threads. We can eager load using global scope. This way, if we only want to fetch threads without the creator, we can do so using `withoutGlobalScopes()`:
```php
<?php
protected static function boot()
{
    parent::boot();

    static::addGlobalScope('replyCount', function ($builder) {
        $builder->withCount('replies');
    });
    static::addGlobalScope('creator', function ($builder) {
        $builder->with('creator');
    });
}
// If we don't want to load the creator when fetching a thread, we can do:
App\Thread::withoutGlobalScopes()->first();
```
# Delete Thread
When deleting a thread, we want to delete the associated replies as well. We also don't want un-authorized user to delete thread. Only the person who created the thread can delete it.
```php
<?php
// Route
Route::delete('threads/{category}/{thread}', 'ForumThreadsController@destroy');
// ForumThreadsController, destroy action
public function destroy($category, ForumThread $thread)
{
    $thread->delete();
    return response([], 204);
}
// ForumThreads, when thread is deleted, its associated replies will be deleted
static::deleting(function ($thread) {
    $thread->replies()->delete();
});
```
## Policy guard
We can create a policy to ensure only authorized user can delete a thread. This can also apply to `update` and `edits` on the thread. Since the permission is the same all around, we can just use the update policy in delete action.
```bash
php artisan make:policy ForumThreadPolicy --model=ForumThread
```
```php
<?php
// In ForumThreadPolicy
public function update(User $user, ForumThread $thread)
{
    return $thread->user_id == $user->id;
}

// Edit AuthServiceProvider to use our new policy
protected $policies = [
    'App\ForumThread' => 'App\Policies\ForumThreadPolicy',
];
// Add authorize check to ForumsThreadController
$this->authorize('update', $thread);
```
#### Test cases
```php
<?php
// Test user can delete thread
function test_a_thread_can_be_deleted()
{
    // Given a signed in user and a thread
    $this->signIn();
    $thread = create('App\ForumThread');

    // When we send a json request to delete a thread
    $response = $this->json('DELETE', $thread->path());
    // Assert we get a status 204 (server fulfilled request) and thread is not in database
    $response->assertStatus(204);
    $this->assertDatabaseMissing('forum_threads', ['id' => $thread->id]);
}

function test_unauthorized_cannot_delete_threads()
{
    // Guest will be redirected if they attempt to delete thread
    $this->withExceptionHandling();
    $thread = create('App\ForumThread');
    $this->delete($thread->path())
        ->assertRedirect('/login');

    // Even if signed in unauthorized user would still be redirected
    $this->signIn();
    $this->delete($thread->path())
        ->assertStatus(403);
}
```

In view, to delete the thread we need to set the *method_field('delete')* as some browsers do not support `delete` request. We can cheat it by stating it's a `post` request, but tell our Laravel server it's actually a `delete` request.

```html
@can('update', $thread)
    <form class="" action="{{ $thread->path() }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <button type="submit" class="btn btn-primary">Delete Thread</button>
    </form>
@endcan
```
# Admin Permission
We can use a `Gate` to set admin permissions.
```php
<?php
// In Auth service provider, before any policies are ran
// If the Gate return true, then it'll override all the policy
public function boot()
{
    $this->registerPolicies();

    Gate::before(function ($user)  {
        if ($user->name === 'Sonya') return true;
    });
}
```
# Record Activity
Anytime a reply or thread is created, we want to record the activity and show it. Create *Activty* model and migration with:  `php artisan make:model Activity -m`
#### Create activty on thread create
```php
<?php
// Setup Schema
Schema::create('activities', function (Blueprint $table) {
    $table->increments('id');
    $table->unsignedInteger('user_id')->index();
    $table->unsignedInteger('subject_id')->index();
    $table->string('subject_type', 50);
    $table->string('type', 50);
    $table->timestamps();
});
```
#### Create Records Activity Trait
Since we want to record activity for threads and replies. We should create a trait. The trait will listen for when a model is created, and it will generate activity record.
```php
<?php
// App\RecordsActivity.php
namespace App;

trait RecordsActivity
{
    // This will be triggered automatically when we do `use RecordsActivity` in model
    protected static function bootRecordsActivity()
    {
        foreach (static::getActivitiesToRecord() as $event) {
            static::$event(function ($model) use ($event) {
                $model->recordActivity($event);
            });
        }
    }

    protected static function getActivitiesToRecord()
    {
        return ['created'];
    }

    protected function recordActivity($event)
    {
        $this->activity()->create([
            'user_id' => auth()->id(),
            'type' => $this->getActivityType($event)
        ]);
    }

    public function activity()
    {
        // morphMany is a polymorphic relation laravel provides, it will auto-add
        // subject_id and subject_type (the model the activity is created from)
        return $this->morphMany('App\Activity', 'subject');
    }

    protected function getActivityType($event)
    {
        $type = strtolower((new \ReflectionClass($this))->getShortName());
        return "{$even}_{$type}";
    }
}
// Trigger recordsActivity in ForumThread
use RecordsActivity;
```
#### Test activity is recorded
```php
<?php
// Unit/ForumActivityTest.php
public function test_it_records_activity_when_a_thread_is_created()
{
    $this->signIn();
    $thread = create('App\ForumThread');
    $this->assertDatabaseHas('activities', [
        'type' => 'created_forum_thread',
        'user_id' => auth()->id(),
        'subject_id' => $thread->id,
        'subject_type' => 'App\ForumThread'
    ]);
    // assert activity subject equals to thread subject
    $activity = Activity::first();
    $this->assertEquals($activity->subject->id, $thread->id);
}

// To get the subject associated with activity, in Activity.php
public function subject()
{
    return $this->morphTo();
}
```
# View Component
Use view component if we are going to duplicate same html. Create component template:
```php
<h3>{{ $heading }}</h3>
<p>{{ $body }}</p>
```
#### Use component
```php
<?php
@component('profiles.activities.activity')
    @slot('heading')
        // input any html we want in $heading
    @endslot
    @slot('body')
        // input any html we want in $body
    @endslot
@endcomponent
```
# Activity Feed
Move activity feed into model and use in controller
```php
<?php
// ProfilesController
public function show(User $user)
{
    return view('profiles.show', [
        'profileUser' => $user,
        'activities' => Activity::feed($user)
    ]);
}
// Activity.php
public static function feed($user, $take = 20)
{
    return static::where('user_id', $user->id)
        ->latest()
        ->with('subject')
        ->take($take)
        ->get()
        ->groupBy(function ($activity) {
            return $activity->created_at->format('Y-m-d');
        });
}
```
#### Test activity feed
Test that we can fetch an activity feed for any user
```php
<?php
function test_it_fetches_a_feed_for_any_user()
{
    // Given a thread and another thread from a week ago
    $this->signIn();
    create('App\ForumThread', ['user_id' => auth()->id()], 2);
    auth()->user()->activities()->first()->update(['created_at' => Carbon::now()->subWeek()]);

    // When we fetch their feed
    $feed = Activity::feed(auth()->user());
    // Then it should return proper format
    $this->assertTrue($feed->keys()->contains(
        Carbon::now()->format('Y-m-d')
    ));
    $this->assertTrue($feed->keys()->contains(
        Carbon::now()->subWeek()->format('Y-m-d')
    ));
}
```
# Delete Activities
When a thread or a reply is deleted, it should also remove its associated activities.
```php
<?php
// RecordsActivity.php
static::deleting(function ($model) {
    $model->activity()->delete();
});
// ForumThread.php
static::deleting(function ($thread) {
    $thread->replies->each->delete();
    // is same as
    $thread->replies->each(function ($reply) {
        $reply->delete();
    });
});
```
To delete activities on associated reply. We need to loop through each `ForumReply` model to delete it in order to trigger the `RecordsActivity` trait.

# Delete Reply
Setup route and controller action to delete reply
```php
<?php
public function destroy(ForumReply $reply)
{
    $this->authorize('update', $reply);

    $reply->delete();
    return back();
}
```
#### Reply Policy
Create a ForumReplyPolicy: `php artisan make:policy ForumReplyPolicy`
```php
<?php
public function update(User $user, ForumReply $reply)
{
    return $reply->user_id == $user->id;
}
```
#### Test case
```php
<?php
function test_unauthorized_users_cannot_delete_replies()
{
    $this->withExceptionHandling();

    $reply = create('App\ForumReply');

    $this->delete("/replies/{$reply->id}")
        ->assertRedirect('login');

    $this->signIn()
    ->delete("/replies/{$reply->id}")
    ->assertStatus(403);
}

function test_authorized_users_can_delete_replies()
{
    $this->signIn();
    $reply = create('App\ForumReply', ['user_id' => auth()->id()]);

    $this->delete("/replies/{$reply->id}")->assertStatus(302);

    $this->assertDatabaseMissing('forum_replies', ['id' => $reply->id]);
}
```
