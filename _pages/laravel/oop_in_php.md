---
title: OOP in PHP
---
<!-- MarkdownTOC -->

* [Basic OOP](#basic-oop)
* [Inheritance](#inheritance)

<!-- /MarkdownTOC -->

# Basic OOP
Object
:  "object" is an instance of a class where the object can contain variables (properties) and functions (methods).

Properties
:  Pieces of data bound to an object.

#### The constructor method
The constructor method, `__construct()`, is called when a new object is created using a `new` keyword. `$this->prop1 = $prop1` means that the value you pass in the `__construct()` function via the `new` keyword is assigned to `$this`, which represents the object you are dealing with and `->prop1` is the actual property of the object.


By creating a new instance using the `new` keyword, you actually call this `__construct()` method, which constructs the object. And that's why we have to pass in some arguments when we create an instance of a class, since this is how the properties get set!

```php
<?php
class Person {
  // Properties of Person
  public $isAlive = true;
  public $firstname;
  public $lastname;
  public $age;

  // Assigning values
  public function __construct($firstname, $lastname, $age) {
  $this->firstname = $firstname;
  $this->lastname = $lastname;
  $this->age = $age;
}

public function greet() {
  return "hello, my name is " . $this->firstname . " " . $this->lastname . ". Nice to meet you! :-)";
}

}
// new keyword means you create a new object  
$teacher = new Person("teacher", "last", 45);
$student = new Person("student", "name", 13);
echo $student->greet();
?>
```

# Inheritance
Inheritance is a way for one class to take on the properties or methods of another class. It **extends** one class to the other.

A **child class** (*subclass*) can override property or method of **parent class** (*superclass*) by creating a new property or method with the same name. Child version of the class will always take precedence over inhertied version.
```php
<?php
class Shape {
  public $sides = true;
}
class Square extends Shape {
  // overides $sides property from Shape
  public $sides = 4;
}
$square = new Square();
echo $square->sides; // 1
```
#### `final` keyword
The ~~final~~ keyword prevents subclasses from modifying parent class.
```php
<?php
class Vehicle {
  final public function drive () {
    Return "I'm driving.";
  }
}
?>
```
