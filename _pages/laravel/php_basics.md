---
title: PHP Basics
---
<!-- MarkdownTOC -->

* [Echo String](#echo-string)
* [Require](#require)
* [Data Structures](#data-structures)
  * [Variables](#variables)
  * [Array](#array)
  * [Associative arrays](#associative-arrays)
  * [Conditionals](#conditionals)
  * [Functions](#functions)
* [Classes](#classes)
* [Connect to mySQL](#connect-to-mysql)

<!-- /MarkdownTOC -->

# Echo String
```php
<?php
  echo "Hello World";
?>
<!-- Or shorthand for views -->
<?= "Hello World";?>

<!-- Print non-string data -->
<?php
  var_dump($array);
?>
```
Run `php filename.php` to view output in command line.

# Require
To seperate logic from view, use `require`:
```php
<?php
  $greeting = "Hello World";
  require 'index.view.php';
?>
```
# Data Structures
## Variables
Use `$` to declare variables.
```php
<?php
  $name = $_GET['name']; # Gets value of 'name' from url '/?name=Jeff'
  echo "Hello " . $name;
  # Or
  echo "Hello {$name}";
?>
```
## Array
```php
<?php
$names = ['Jeff', 'Mary', 'Peter'];
# Print out each item in array
foreach ($names as $name) {
  echo $name . ', ';
}

# Add to array
$names[] = 'Jane';
?>

<!-- Shorthand for views -->
<?php foreach ($names as $name) : ?>
  <li><?= $name; ?></li>
<?php endforeach; ?>
```
## Associative arrays
```php
<?php
$person = [
  'age' => 31,
  'gender' => 'male'
]
# Print value
foreach ($person as $feature) {
  echo "{$feature}";
}
# Print key and value
foreach ($person as $key => $val) {
  echo "{$key}: {$val}";
}

# Add key and value to array
$person['name'] = 'Jeff';

# Remove from array
unset($person['age']);
```
## Conditionals
```php
<?php
if (condition) {
  echo 'do something';
} else {
  echo 'something else';
}
# tenary operator
condition ? 'true' : 'false';

# evaluate false
if (! true) {  
}
?>

<!-- In view -->
<?php if (condition) : ?>
  <span>do something</span>
<?php else : ?>
  <span>something else </span>
<?php endif; ?>
```
## Functions
```php
<?php
function name($param1, $param2) {
}
# Common die dump function
function dd($data) {
  echo '<pre>';
  die(var_dump(#data)); # die() stops the program from running
  echo '</pre>';
}
dd($array);
```
# Classes
```php
<?php
class Task {
  protected $description; // only the current object can access this property
  protected $completed = false;

  public function __construct($description) {
    // Auto triggered when called/instantiation
    $this->description = $description;
  }
}
new Task('Go to the store'); // trigger __construct() immediately;
```
`new Task()` creates an instance of the class `Task`. An instance of a class is an ~~object~~. `$this->description` sets up a new property on the object.

#### Static method
```php
<?php
class Connection {
  public static function makek() {

  }
}
Connection::make(); // calls the static method make()
```
A static method is useful when we don't require an instantiation of that class. So if we don't use `static` we would need to first instantiate the class then execute the method, like so:
```php
<?php
$task = new Task();
$task->make();
```

# Connect to mySQL
```php
<?php
// Connect to mySQL database
try {
  $pdo = new PDO('mysql:host=127.0.0.1;dbname=databasename', 'user', 'password');
} catch (PDOExceiption $e) {
  die('Could not connect.');
}
// Fetch the query
$statement = $pdo->prepare('select * from todos');
$statement->execute();
// grabs all query into an associative array
$tasks = $statement->fetchAll(PDO::FETCH_OBJ); // 0 => object{}, 1 => object{}

// Print the description of the first task
echo $tasks[0]->description

// Initiate all tasks as Task object
$tasks = $statement->fetchAll(PDO::FETCH_CLASS, 'Task');

class Task {
  public $description;
  public $completed;
  // now we can create methods to use for each task!
  public function completed() {
    $this->completed = true
  }
}
```
