---
title: Forum
---
<!-- MarkdownTOC -->

* [Create Thread](#create-thread)
* [Authentication](#authentication)
* [View Threads](#view-threads)
    * [Setup Test Database](#setup-test-database)
    * [Test view thread title](#test-view-thread-title)
* [Show Replies](#show-replies)
* [Unit Test Replies](#unit-test-replies)
* [Create Thread](#create-thread-1)
* [Test Helpers](#test-helpers)
* [Forum Category](#forum-category)
    * [Filter by category](#filter-by-category)
* [Form Validation](#form-validation)
* [Filter by User](#filter-by-user)
* [Global Query Scope](#global-query-scope)
* [Paginate](#paginate)
* [Sort by Popular Threads](#sort-by-popular-threads)

<!-- /MarkdownTOC -->

# Create Thread
#### Forum Structure
A simple forum will have a thread, reply, and users. So the relationship would be:
- Thread is created by a user
- A reply belongs to a thread, and belongs to user.

```bash
php artisan make:model Thread -mr
php artisan make:model Reply -mc
```
#### Setup Schema
```php
<?php
Schema::create('threads', function (Blueprint $table) {
    $table->increments('id');
    $table->string('title');
    $table->text('body');
    $table->integer('user_id');
    $table->timestamps();
});
Schema::create('replies', function (Blueprint $table) {
    $table->increments('id');
    $table->integer('thread_id');
    $table->integer('user_id');
    $table->text('body');
    $table->timestamps();
});
```
#### Create dummy content
Use `ModelFactory.php` to create user and thread
```php
<?php
$factory->define(App\ForumThread::class, function (Faker\Generator $faker) {
    return [
        'user_id' => function() {
            return factory('App\User')->create()->id;
        },
        'title' => $faker->sentence,
        'body' => $faker->paragraph
    ];
});
$factory->define(App\ForumReply::class, function (Faker\Generator $faker) {
    return [
        'thread_id' => function() {
            return factory('App\Thread')->create()->id;
        },
        'user_id' => function() {
            return factory('App\User')->create()->id;
        },
        'body' => $faker->paragraph
    ];
});

```
#### Create factory threads/replies
```php
<?php
// Create 10 threads in console
$threads = factory('App\ForumThread', 10)->create();
// For each thread, create 3 replies with thread_id
$threads->each(function ($thread) { factory('App\ForumReply', 3)->create(['forum_thread_id' => $thread->id]); });
```
# Authentication
Create `Authentication` scafolding. This generates controllers and views for user login/registration
```bash
php artisan make:auth
```
# View Threads
```php
<?php
// Create route
Route::get('/threads', 'ForumThreadsController@index');
Route::get('/threads{id}', 'ForumThreadsController@show');
// Setup ForumThreadsController
function index()
{
    $threads = ForumThread::latest()->get();
    return view('threads.index', compact('threads'));
}
public function show(ForumThread $thread)
{
    return view('threads.show', compact('thread'));
}
// In ForumThread model, create path function so we can return the title as url path
public function path()
{
    return '/thread/' . $this->id;
}

```
## Setup Test Database
```php
<?php
// At bottom of phpunit.xml, insert the following to create database in memory
<php>
    <env name="APP_ENV" value="testing"/>
    <env name="DB_CONNECTION" value="sqlite"/>
    <env name="DB_DATABASE" value=":memory:"/>
</php>
```
## Test view thread title
We should create a test case where the title must be in view.

```php
<?php
// Create test file in /Tests/Feature/ForumThreadsTest.php
namespace Tests\Feature;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ReadForumThreadsTest extends TestCase
{
    use DatabaseMigrations; // Creates database in memory, so it will delete whatever is created in test cases

    public function setUp()
    {
        parent::setUp();
        $this->thread = factory('App\ForumThread')->create();
    }
    public function test_a_user_can_view_all_threads()
    {
       $response = $this->get('/threads')
                    ->assertSee($this->thread->title);


    }
    public function test_a_user_can_read_single_thread()
    {
       $response = $this->get('/threads/' . $this->thread->id)
                    -> assertSee($this->thread->title);
    }
}
```
# Show Replies
Declare relationship between replies and thread
```php
<?php
// In ForumThread.php
public funciton replies()
{
    return $this->hasMany(ForumReply::class);
}
```
#### Associated replies to owner
```php
<?php
// In ForumReply
public function owner()
{
    $this->belongsTo(User::class, 'user_id');
}
```
# Unit Test Replies
#### Test replies has an owner/creator
Create a test using artisan: `php artisan make:test ForumReplyTest`
```php
<?php
function test_reply_has_an_owner()
{
    // Given that I have a thread
    $reply = factory('App\Reply')->create();
    // Then when I fetch the thread's owner, it should be an instance of User
    $this->assertInstanceOf('App\User', $reply->owner);
}
```
#### Test a thread has replies and owner
```php
<?php
// Unit/ForumThreadTest
function test_thread_has_replies()
{
    $thread = factory('App\ForumThread')->create();
    $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $thread->replies);
}

function test_thread_has_creator()
{
    $thread = factory('App\ForumThread')->create();
    $this->assertInstanceOf('App\User', $thread->owner);
}

```
```bash
# Use filter to test in console
 phpt --filter test_thread_has_creator
```
We should test that an authenticated user can participate in replies
#### Create a reply
```php
<?php
// Setup Routes
Route::post('/threads/{thread}/replies', 'ForumRepliesController@store');

// Add store action in ForumRepliesController
public function store(ForumThread $thread)
{
    $thread->addReply([
        'body' => request('body'),
        'user_id' => auth()->id()
    ]);
    return back();
// In ForumThread model
public function addReply($reply)
{
    $this->replies()->create($reply);
}
// Add Model.php and add below to prevent mass assignment exception
protected $guarded = [];
```
#### Create test to post reply
Create test case where only an authenticated user can post replies
```php
<?php
use DatabaseMigrations;
// expect exception if user is not authenticated
function test_unauthenticated_users_may_not_add_replies()
{
    $this->expectException('Illuminate\Auth\AuthenticationException');

    $this->post('/threads/1/replies', []);
}

function test_an_authenticated_user_may_participate_in_forum_threads()
{
    // Give we have an authenticated user
    $user = factory('App\User')->create();
    $this->be($user);

    // And an existing thread
    $thread = factory('App\ForumThread')->create();

    // When the user adds a reply to the thread
    $reply = factory('App\ForumReply')->make();
    $this->post($thread->path().'/replies', $reply->toArray());

    // Their reply should be visible on the page
    $this->get($thread->path())
        ->assertSee($reply->body);
}
```

# Create Thread
```php
<?php
// set resourceful route, this will generate route to all common actions like:
// index, show, create, delete, detroy
Route::resource('threads', 'ThreadsController');
// In ForumThreadController
public function __construct()
{
    // user must be authenticated, applies only to store action
    $this->middleware('auth')->only('store');
}

public function store(Request $request)
{
    $thread = ForumThread::create([
        'user_id' => auth()->id(),
        'title' => request('title'),
        'body' => request('body')
    ]);
    return redirect($thread->path());
}
```

Make a test so only authenticated user can create new forum thread.
```php
<?php
function test_guest_may_not_create_thread()
{
    $this->expectException('Illuminate\Auth\AuthenticationException');
    $this->post('/threads', []);
}
function test_an_authenticated_user_can_create_new_forum_thread()
{
    // Given a signed in user
    $user = factory('App\User')->create();
    $this->be($user);
    // When we hit endpoint to create new thread
    $thread = factory('App\ForumThread')->make();
    $this->post('/threads', $thread->toArray());

    // Then when we visit the thread page
    // We should see the new thread
    $this->get($thread->path())
        ->assertSee($thread->title)
        ->assertSee($thread->body);
}
function test_cannot_see_the_create_thread_page()
{
    $this->withExceptionHandling()->get('/threads/create')
        ->assertRedirect('/login');
}
```

# Test Helpers
We can make helpers so we don't have to always type code to `use DatabaseMigrations;`, sign in a user, create threads.
#### Dummy content helper
```php
<?php
// In composer.json, under autoload-dev add:
"files": ["tests/utilities/functions.php"]
// then run composer dump-autload

// Create the file tests/utilities/functions.php
function create($class, $attributes = [])
{
    return factory($class)->create($attributes);
}
function make($class, $attributes = [])
{
    return factory($class)->make($attributes);
}
```
Now anytime we do something like `$thread = factory('App\ForumThread')->create();`. We can now do
`$thread = create('App\ForumThread');`.

#### Sign in helper
```php
<?php
// In TestCase.php
protected function signIn($user = null)
{
    $user = $user ? : create('App\User');
    $this->actingAs($user);
    return $this;
}

// To use it, do $this->signIn();
```
#### Extend TestCase
Extend testcase to use `databasemigration` in all  new classes we create
```php
<?php
// Create new file tests/DatabaseTestCase.php
namespace Tests;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class DatabaseTestCase extends TestCase
{
    use DatabaseMigrations;
}

// Then just use the following in all test classes
namespace Tests\Feature;

use Tests\TestCase;
use Tests\DatabaseTestcase;

class ReadForumThreadsTest extends DatabaseTestCase
```

# Forum Category
Generate model and migration with `php artisan make:model ForumCategory -m`. Add in `forum_category_id` in create_forum_threads_table migration. Then we can add dummy content through `ModelFactory.php`.
```php
<?php
// define ForumThread, add in
'forum_category_id' => function() {
    return factory('App\ForumCategory')->create()->id;
},

$factory->define(App\ForumCategory::class, function (Faker\Generator $faker) {
    $name = $faker->word;
    return [
        'name' => $name,
        'slug' => $name
    ];
});

// Define the relationship in ForumThread
public function category()
{
    return $this->belongsTo(ForumCategory::class, 'forum_category_id');
}
```

Create test case to check instance of forum category on thread create
```php
<?php
function test_a_thread_belongs_to_a_category()
{
    $thread = create('App\ForumThread');
    $this->assertInstanceOf('App\ForumCategory', $thread->category);
}
// On thread create, we should store the category id as well.
// In ForumThreadsController store action, add in
'forum_category_id' => request('forum_category_id'),
```
#### Route thread path to category
We want our rate to show threads in category as such -> `/threads/category/1`.
```php
<?php
// In ForumThread model, change path() to
public function path()
{
    return "/threads/{$this->category->slug}/{$this->id}";
}

// Create test in ForumThreadsTest
function test_a_thread_can_make_a_string_path()
{
    $thread = create('App\ForumThread');
    $this->assertEquals(
        "/threads/{$thread->category->slug}/{$thread->id}", $thread->path()
    );
}
```
## Filter by category
View threads filtered by categories
```php
<?php
// Set route to view /threads/category
Route::get('/threads/{category}', 'ForumThreadsController@index');
// Ensure we get the slug in URL not id, in ForumCategory
public function getRouteKeyName()
{
    return 'slug';
}

// Setup thread filter in index action
public function index(ForumCategory $category)
{
    if ($category->exists) {
        $threads = $category->threads()->latest()->get();
    } else {
        $threads = ForumThread::latest()->get();
    }
    return view('threads.index', compact('threads'));
}
```
Add tests to assert we see all the correct threads in a category, as well as assert we don't see the ones that don't belong in the category.

```php
<?php
// In ReadForumThreadsTest
function test_a_user_can_filter_threads_according_to_a_category()
{
    $category = create('App\ForumCategory');
    $threadInCategory = create('App\ForumThread', ['forum_category_id' => $category->id]);
    $threadNotInCategory = create('App\ForumThread');

    $this->get('/threads/'.$category->slug)
        ->assertSee($threadInCategory->title)
        ->assertDontSee($threadNotInCategory->title);
}
```

# Form Validation
Add validation for when form is submitted, title and body are not null, and forum category is not nil or doesn't exists
```php
<?php
$this->validate($request, [
    'title' => 'required',
    'body' => 'required',
    'forum_category_id' => 'required|exists:forum_categories,id'
]);

// In CreateThreadsTest assertError when title is null
public function publishThread($overrides = [])
{
    $this->withExceptionHandling()->signIn();
    $thread = make('App\ForumThread', $overrides);

    return $this->post('/threads', $thread->toArray());
}

function test_a_thread_requires_a_title()
{
    $this->publishThread(['title'=>null])
        ->assertSessionHasErrors('title');
}
// do same thign as above to validate body

function test_a_thread_requires_a_category_id()
{
    factory('App\ForumCategory', 2)->create();

    $this->publishThread(['forum_category_id'=>null])
        ->assertSessionHasErrors('forum_category_id');
    // We created only 2 categories, so we know for sure id 999 does not exists, so it should return an exception
    $this->publishThread(['forum_category_id'=>999])
        ->assertSessionHasErrors('forum_category_id');
}
```
# Filter by User
Show threads created by signed in user when they visit url `threads?by=UserName`. We should refactor the index action in `ForumThreadsController` so we can add more filters in the future.
```php
<?php
// In ForumThreadsController index, we may want to get filtered result like this
public function index(ForumCategory $category, ForumThreadFilters $filters)
{
    $threads = $this->getThreads($category, $filters);
    return view('threads.index', compact(['threads', 'category']));
}
protected function getThreads(ForumCategory $category, ForumThreadFilters $filters)
{
    $threads = ForumThread::latest()->filter($filters); // filter fetches from ForumThread model
    if ($category->exists) {
        $threads->where('forum_category_id', $category->id);
    }
    return $threads->get();
}
// Create a query scope to filter in ForumThread
public function scopeFilter($query, $filters)
{
    return $filters->apply($query); // apply() will be defined in Filters.php
}

// Create app/filters/ForumThreadFilters.php
namespace App\Filters;
use App\User;

class ForumThreadFilters extends Filters
{
    protected $filters = ['by'];
    /**
    * Filter the query by a given username.
    * @param string $username
    * @return mixed
    */
    protected function by($username)
    {
        $user = User::where('name', $username)->firstOrFail();
        return $this->builder->where('user_id', $user->id);
    }
}
// Create a general filter class in filters/Filters.php

namespace App\Filters;
use Illuminate\Http\Request;

abstract class Filters
{
    protected $request, $builder;
    protected $filters = [];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply($builder)
    {
        $this->builder = $builder;

        foreach ($this->getFilters() as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }

        return $this->builder;
    }

    /**
    * @return ['by' => 'username']
    */

    public function getFilters()
    {
        return $this->request->only($this->filters);
    }
}

```
Test the filter works
```php
<?php
// ReadForumThreadsTest
function test_a_user_can_filter_threads_by_any_username()
{
    $this->signIn(create('App\User', ['name' => 'JohnDoe']));
    $threadByJohn = create('App\ForumThread', ['user_id' => auth()->id()]);
    $threadNotByJohn = create('App\ForumThread');

    $this->get('/threads?by=JohnDoe')
        ->assertSee($threadByJohn->title)
        ->assertDontSee($threadNotByJohn->title);
}
```
# Global Query Scope
We want to show the reply count globally, we can add a global query scope.

Global Query Scope
:  A global scope is just a query scope that is automatically applied to all queries. So whenever we call Thread::all() or first() etc... the `replies_count` will automatically be added on the return object.
```php
<?php
// boot() is something Laravel will know to trigger automatically
protected static function boot()
{
    parent::booth();
    // this is where we can add a global query scope
    static::addGlobalScope('replyCount', function ($builder) {
        $builder->withCount('replies');
    });
}
// Now in view, we can call the replies count
$thread->replies_count
```
# Paginate
We can add pagination for when there's too many replies. Laravel makes it super easy!

```php
<?php
// In ForumThreadsController, show action
return view('threads.show', [
    'thread' => $thread,
    'replies' => $thread->replies()->paginate(25)
]);

// Then we can show the pagination link
{{ $replies->links() }}
```

# Sort by Popular Threads
Threads with the most replies are the most popular. When we filter by `/threads?popular=1`, it should order the threads by most replies. Add popular filter in `ForumThreadsFilter.php`

```php
<?php
protected $filters = ['by', 'popular'];
protected function popular()
{
    $this->builder->getQuery()->orders = [];
    return $this->builder->orderBy('replies_count', 'desc');
}
```

Add test case to ensure we return the threads in most popular order

```php
<?php
// ReadForumTests.php
function test_a_user_can_filter_threads_by_popularity()
{
    // Given three threads
    // With 2 replies, 3 replies and 0 respectively.
    $threadWithTwoReplies = create('App\ForumThread');
    create('App\ForumReply', ['forum_thread_id' => $threadWithTwoReplies->id], 2);

    $threadWithThreeReplies = create('App\ForumThread');
    create('App\ForumReply', ['forum_thread_id' => $threadWithThreeReplies->id], 3);

    $threadWithNoReplies = $this->thread;

    // When I filter all threads by popularity
    $response = $this->getJson('threads?popular=1')->json();

    // They should be returned from most replies to least.
    $this->assertEquals([3, 2, 0], array_column($response, 'replies_count'));
}
```
