---
title: Laravel Blog
---
<!-- MarkdownTOC -->

* [Larvel Basics](#larvel-basics)
* [Layouts and Partials](#layouts-and-partials)
* [Generate Post model and controller](#generate-post-model-and-controller)
* [Show all posts](#show-all-posts)
* [Create Post](#create-post)
* [Form Validation](#form-validation)
* [Show a single post](#show-a-single-post)
* [Add Comments to post](#add-comments-to-post)
* [Add user to post](#add-user-to-post)
* [Setup User registration](#setup-user-registration)
* [User Login](#user-login)
* [Add filter by archive](#add-filter-by-archive)
* [View Composer](#view-composer)
* [Unit Testing](#unit-testing)
    * [Model Factory](#model-factory)
* [Send Email](#send-email)
    * [Create Email](#create-email)
* [Form Requests](#form-requests)
* [Flash and Error Message](#flash-and-error-message)
* [Tagging](#tagging)
    * [Add tag to post](#add-tag-to-post)
    * [Get Tag by name](#get-tag-by-name)
    * [Show tags on sidebar](#show-tags-on-sidebar)

<!-- /MarkdownTOC -->

# Larvel Basics
#### Php Artisan Commands
```bash
# Create a migration with table setup
php artisan make:migration create_table_name_table --create=<table-name>
php artisan migrate

# Create model and table
php artisan make:model Task -m

# Dump autoload to recompile with composer
composer dump-autoload

# Go into the console of laravel
php artisan tinker

# Roll back and rerun all migrations
php artisan migrate:refresh
```
# Layouts and Partials
To use a layout, create file in `views/layouts/master.blade.php`.
```php
<?php
<body>
    @include('layouts.nav')
    <div class="container">
        @yield('content')
    </div>

    @include('layouts.footer')
</body>
```
`@include` renders the partial in `views/layouts/nav.blade.php`. `@yield` creates a placeholder for `content`.

To use this layout, in other files you would do this:
```php
<?php
@extends('layouts.master')

@section('content')
    <p>Body of page </p>
@endsection
```

# Generate Post model and controller
```bash
php artisan make:model Post -mc
```
`-mc` creates a migration file as well as controller

#### Create `Post` table
Open and edit the migration file generated:
```php
<?php
public function up()
{
    Schema::create('posts', function (Blueprint $table) {
        $table->increments('id');
        $table->string('title');
        $table->text('body');
        $table->timestamps();
    });
}
```
Then run `php artisan migrate` to create table in database.

# Show all posts
Setup routes in `routes/web.php`
```php
<?php
Route::get('/', 'PostsController@index');
```
Add `index` action in `PostsController`
```php
<?php
public function index()
{
    $posts = Post::all();
    // OR to order by newest post
    $posts = Post::latest()->get();
    return view('posts.index', compact('posts'));
}
```
Then create the view file with file name `views/posts/index.blade.php`.
```php
<?php
<ul>
@foreach ($posts as $post)
            <li>
                <a href="/posts/{{ $post->id }}">{{ $post->title }}</a>
                - {{ $post->created_at->toFormattedDateString() }}
            </li>
        @endforeach
</ul>
```
`toFormattedDateString()` formats the date to -> Jan 17, 2017. Check the php [Carbon library](http://carbon.nesbot.com/docs/) for more options.
# Create Post
#### Setup Route
Setup route to show the create form and POST request n `routes/web.php`:
```php
<?php
Route::get('/posts/create', 'PostsController@create');
Route::post('/posts', 'PostsController@store');
```
#### Show create form
Set up create form in `view/posts/create.blade.php`:
```php
<?php
<h1>Publish a Post</h1>
<form class="" action="/posts" method="post">
    {{ csrf_field() }}
    <label>Title</label>
    <input type="text" name="title" value=""><br />
    <label>Body</label>
    <textarea name="body"></textarea>
    <br />
    <button type="submit">Publish</button>
</form>
```
The `csrf_field()` protects us from cross origin attacks. A token gets generated in our server and when the form gets submitted, it matches the token against the one generated in server.

#### Add `create` and `store` action in Controller
Setup controller action to show the create form and `store` the form data, `PostsController.php`:
```php
<?php
public function create()
{
    return view('posts.create');
}

public function store()
{
    Post::create(request(['title', 'body'])); // only permits 'title' and 'body' to be submitted
    return redirect('/');
}
```
**Note:** Add `use App\Post;` at the top of the controller file for namespacing.

A different way to create a post:
```php
<?php
$post = new Post;
$post->title = "Title of Post";
$post->body = "Body of post";
$post->save();
```

#### Add additional security in model
For more security, it is good idea to guard certain values from being changed through POST requests. In `Model.php` (create one if it doesn't exist):
```php
<?php
use Illuminate\Database\Eloquent\Model as Eloquent; // set to eloquent

class Model extends Eloquent // set to eloquent
{
    protected $guarded = []; // Can input $user_id etc...
}
```
# Form Validation
We can validate form submission in the controller when form gets submitted, in the `store()` function:
```php
<?php
$this->validate(request(), [
            'title' => 'required',
            'body' => 'required'
        ]);
```
#### Show error
~~View the documentation for more validation options!~~ This will redirect the page back to the form and output an error. We can show the error in view
```php
<?php
@if (count($errors))
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
@endif
```

# Show a single post
#### Setup routes and controller
```php
<?php
// In routes/web.php
Route::get('/posts/{post}', 'PostsController@show');

// In PostsController.php
public function show()
{
    $post = Post::find($id);
    return view('posts.show', compact('post'));
}

// Shorter way
public function show(Post $post)
{
    return view('posts.show', compact('post'));
}
```
Then render the view by creating view file `views/posts/show.blade.php`

# Add Comments to post
```bash
php artisan make:model Comments -m
```
#### Create Comments tabel
A `comment` belongs to a `post`. So we need to include a refrence to post using `post_id`:
```php
<?php
Schema::create('comments', function (Blueprint $table) {
    $table->increments('id');
    $table->integer('post_id'); // Comment belongs to a post
    $table->string('body');
    $table->timestamps();
});
```

#### Declare relationship in model
```php
<?php
// In Post.php
public function comments() {
    return $this->hasMany(Comment::class);
}
// To get post comment
$post = Post::find(4)
$post->comments

// In Comment.php
public function post()
{
    return $this->belongsTo(Post::class);
}
// To get post from comment
$comment = Comment::first();
$comment->post;
```
#### Show comment in post
```php
<?php
@foreach ($post->comments as $comment)
    <p>{{ $comment->body }}</p>
@endforeach
```

#### Add comment to post
Add a form, notice the action field:
```html
<form class="" action="/posts/{id}/comments" method="post">
    <textarea name="body" rows="8" cols="80" placeholder="Your response here"></textarea>
    <button type="submit">Add Comment</button>
</form>
```
Setup routes `Route::post('/posts/{post}/comments', 'CommentsController@store');`. Then add CommentsController `php artisan make:controller CommentsController`. In `CommentsController.php`:

```php
<?php
// Make sure we add this at the top of the file
use App\Post;
use App\Comment;

public function store(Post $post)
{
    // valide comment, comment must be entered and with minimum 2 characters
    $this->validate(request(), ['body' => 'required|min:2']);
    Comment::create([
        'body' => request('body'),
        'post_id' => $post->id
    ]);
    return back();
}
```
Or for better clarity we can do this:
```php
<?php
// In CommentsController, change Comment::create to
$post->addComment(request('body'));

// Then add the method to Post.php model
public function addComment($body)
{
    Comment::create([
        'body' => $body,
        'post_id' => $this->id
    ]);
    // Or cleaner method
    $this->comments()->create(compact('body'));
}
```
# Add user to post
Go to `create_posts_table` migration and add `$table->integer('user_id')`. Then add the same thing to `create_comments_table` and run `php artisan migrate:refresh`. ***NOTE*** This will reset all your data!

#### Add relationship to models
```php
<?php
// In Comment.php
public function user() // $comment->user->name
{
    return $this->belongsTo(User::class);
}
// In Post.php
public function user()
{
    return $this->belongsTo(User::class);
}
// In User.php
public function posts()
{
    return $this->hasMany(Post::class);
}
```
# Setup User registration
```php
<?php
// Setup routes
Route::get('/register', 'RegistrationController@register');
Route::post('/register', 'RegistrationController@store')

// Then create RegistrationController and SessionsController

//In RegistrationController
public function register()
{
    // Show register form in /register
    return view('sessions.create');
}
public function store() {
    // Create user in table after form submission

    // Validate data
    $this->validate(request(), [
        'name' => 'required',
        'email' => 'required|email', // Is of email type
        'password' => 'required|confirmed' // matches password_confirmed field
    ]);

    // Create the user in table
    $user = User::create(request(['name', 'email', 'password']));
    // Login the user
    auth()->login($user);

    // Redirect to home
    return redirect()->home();
    // to use home() we must name our route
    Route::get('/', 'PostsController@index')->name('home');

}
```
#### User authentication
```php
<?php
// Show user name in views
@if (Auth::check())
    <p>{{ Auth::user()->name }}</p>
@endif

// Only allow logged in user to create post, in PostController
public function __construct()
{
    // block all routes access to not logged in users, exccept for index and show method
    $this->middleware('auth')->except(['index', 'show']);
}
```
#### Use the User model to create posts
A different way to create posts by using the `User` model
```php
<?php
// in PostController
public function store()
{
    $this->validate(request(), [
        'title' => 'required',
        'body' => 'required',
    ]);
    auth()->user()->publish(
        new Post(request(['title', 'body']))
    );

    return redirect('/');

}
// in User.php
public function publish(Post $post)
{
    $this->posts()->save($post);
}
```
# User Login
```php
<?php
// Create routes
Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');

// In SessionsController
public function create()
{
    return view('sessions.create');
}
// Then create login form in /views/sessions/create.blade.php

// Store the new sessions on login form post
public function store()
{
    // Matches email and password to database
    if (! auth()->attempt(request(['email', 'password']))) {
        return back(); // if no match, redirect back
    }

    return redirect()->home();
}
// Logout will trigger session destroy
public function destroy()
{
    auth()->logout();
    return redirect()->home();
}
// Let only guests can see login form, otherwise redirect to home
public function __construct()
{
    $this->middleware('guest');
}
```
# Add filter by archive
An archive collects all posts and separates each by year and month, like -> March 2014, Feb 2014. We can group posts by performing a `sql` query.

```sql
/*The following query will group posts by month and year -> March 2017, Feb 2016 etc...*/
select
    year(created_at) year,
    monthname(created_at) month,
    count(*) published
from posts
group by year, month
```
#### Filter posts
We need to grab posts based on filter request `/?month=March&year=2017`. We can apply a filter like `Post::where(...)`. But if we want to use this query in many places, we can DRY the code up by using a ~~scope~~.

Scope
:  Scopes allow you to define common sets of constraints that you may easily re-use throughout your application.
```php
<?php
// Example, in model
public function scopePopular($query)
{
   return $query->where('votes', '>', 100);
}
// Then we can call this anywhere through our application
$users = App\User::popular()->->get();
```
```php
<?php
// In Post.php, get posts created in month and year
public function scopeFilter($query, $filters)
{
    if ($month = $filters['month']) {
        $query->whereMonth('created_at', Carbon::parse($month)->month);
    }
    if ($year = $filters['year']) {
        $query->whereYear('created_at', $year);
    }
}

// In Postscontroller, we can use the filter like so
// Get filtered posts when a request is made with /?month=March&year=2017
$posts = Post::latest()
        ->filter(request(['month', 'year']))
        ->get();


```
#### Group posts
Group post using query
```php
<?php
// In Post.php, select and group posts by month and year ordered by latest first.  Make sure to add use Carbon\Carbon on top of file
public static function archives()
{
    return static::selectRaw('year(created_at) year, monthname(created_at) month, count(*) published')
        ->groupBy('year', 'month')
        ->orderByRaw('min(created_at) desc')
        ->get()
        ->toArray();
}
// In Postsontroller index action, get the groupings. Ex. Mar 2017, Feb 2016
$archives = Post::archives();

// To show in view with link for filter request
@foreach ($archives as $dates)
    <li><a href="/?month={{ $dates['month'] }}&year={{ $dates['year'] }}">{{ $dates['month'] }} {{$dates['year']}}</a></li>
@endforeach
```
# View Composer
If we want to make `archives` available everywhere without having to pass in `$archives` to every action. We can make use of the `app/Providers/AppServiceProvider.php`. **`view()->composer()`** allows us to hook into any views.
```php
<?php
// The boot method gets called when Laravel framework is loaded
public function boot()
{

    // We want to make the archives available to any views that has a sidebar. So we check if view has sidebar, and use a callback function
    view()->composer('layouts.sidebar', function ($view) {
        $view->with('archives', \App\Post::archives()); // with() is exact same as using compact()
    });
    // Then show archives in the layout
}
```

# Unit Testing
We can use test unit in the console
```bash
# here's a test from `ExampleTest` file
 vendor/bin/phpunit tests/Feature/ExampleTest.php
```
We can test if an element exists on page
```php
<?php
public function testBasicTest()
{
    // make a GET request to homepage, and assert we see the text 'Home'
    $this->get('/')->assertSee('Home');
}
```
Let's write a test to ensure the **archives** always works! Here's a common method to write tests
```php
<?php
use App\Post;
use DatabaseTransaction; // runs tests an deletes created records in tests
public function testBasicTest()
{
    // In pseudo-code
    // Give - I have two records in the database that are posts, and each one is posted a month apart.
    // read below to learn about factory
    $first = factory(Post::class)->create();
    $second = factory(Post::class)->create([
        'created_at' => \Carbon\Carbon::now()->subMonth() // sets created_at to previous month
    ]);
    // When - I fetch the archives.
    $posts = Posts::archives();

    // Then - the response should be in the proper format
    // I expect to receive a count of 2 items and compare that against the posts
    $this->assertCount(2, $posts);

    // A bit more specific
    $this->assertEquals([
        [
            "year" => $first->created_at->format('Y'),
            "month" => $first->created_at->format('F'),
            "published" => 3
        ],
        [
            "year" => $second->created_at->format('Y'),
            "month" => $second->created_at->format('F'),
            "published" => 3
        ]
    ], $posts);
}
```
`Given` sets up the world for our tests. `When` hits the method or performs the action. `Then` creates the assertion of the result we want to see.

## Model Factory
To set up the world for testing, Laravel provides something called **`factory`** in `database/factories/ModelFactory.php`. A **`factory`** is like a blueprint for *Eloquent* model. This way we can quickly whip up new records or save to database by using dummy data. The `ModelFactory` already contains a factory to create a dummy user, let's use it in the console:
```php
<?php
// creates a user with fake name and email, but it's not saved
factory('App\User')->make();
// if we want to save the dummy user, we can use create instead
factory('App\User')->create();
// this will give us 50 different users
factory('App\User', 50)->make();
```
Create a factory to generate fake posts, in `ModeFactory.php`:
```php
<?php
$factory->define(App\Post::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'user_id' => function() {
            return factory(App\User::class)->create()->id; // Creates new user and returns id
        },
        'title' => $faker->sentence,
        'body' => $faker->paragraph,
    ];
});
// We can then call it in console
factory(App\Post::class)->make();
```

# Send Email
We can send a welcome email to newly registered user
```php
<?php
// In RegistrationController store action, use the Mail facade and send a email to user
\Mail::to($user)->send(new Welcome);
```
## Create Email
Run `php artisan make:mail Welcome` in console. Then import the new mail `use App\Mail\Welcome;` to `RegistrationController`. In `Mail/Welcome.php`:
```php
<?php
use App\User;
class Welcome extends Mailable
{
    public $user;
    use Queueable, SerializesModels;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this->view('emails.welcome');
        // Create email content at /views/emails/welcome.blade.php
    }
}
```
#### Setup smtp
Use Gmail smtp server to send email
```php  
MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=587
MAIL_USERNAME=gmail email address
MAIL_PASSWORD=gmailpassword
MAIL_ENCRYPTION=tls
```
# Form Requests
If there are lots of form input fields, it is a good idea to extract all the form validation logic into a dedicated **form object**. We can create a form object to store validation and check authorization to post form request.
```bash
php artisan make:request RegistrationForm
```
Move validation from the store action in RegistrationController to the new RegistrationForm class.
```php
<?php
// In RegistrationForm, set validation rules
public function rules()
{
    return [
        'name' => 'required',
        'email' => 'required|email',
        'password' => 'required|confirmed'
    ];
}
// Set to true, so anyone can make this registration
public function authorize()
{
    return true;
}

// Import to RegistrationController
use App\Http\Requests\RegistrationForm;
public function store(RegistrationForm $request) {
    // ...
}
// Now when user makes a registration, it will hit the store Laravel will immediately
```
Now when user makes a registration, it will first hit the `store` action. Then Laravel will immediately validate the form in `RegistrationForm`. If there are errors, it will automatically redirect back to previous page with error messages **without** executing anything inside the `store` function.

We can move all the logic for submitting the form inside `RegistrationForm` as well:
```php
use App\User;
use App\Mail\Welcome;
use Illuminate\Support\Facades\Mail;
<?php
public function persist()
{
    $user =  User::create(
        $this->only(['name', 'email', 'password'])
    );
    auth()->login($user);

    Mail::to($user)->send(new Welcome($user));
}
// Update our RegistrationController
public function store(RegistrationRequest $request) {
    $form->persist();
    return redirect()->home();
}
```

# Flash and Error Message
```php
<?php
// Setup in controller where you want to notify a user
session()->flash('message', 'Thanks for signing up!');
// Then render flash mssg
@if ($flash = session('message'))
    {{ $flash }}
@endif
```

# Tagging
A post can have many tags, and a tag can belong to many posts. This is a many-to-many relationship. So we need to create a third table, the pivot table, to track the relationship. The table names shall be `tags, posts, and post_tag`. Generate `Tag` model and migration:
```bash
php artisan make:model Tag -m
```

```php
<?php
Schema::create('tags', function (Blueprint $table) {
    $table->increments('id');
    $table->string('name')->unique();
    $table->timestamps();
});

Schema::create('post_tag', function (Blueprint $table) {
    $table->integer('post_id');
    $table->integer('tag_id');
    $table->timestamps();

    // Make sure the COMBINATION post_id and tag_id is unique
    $table->primary(['post_id', 'tag_id']);
});

Schema::dropIfExists('tags');
Schema::dropIfExists('post_tag');
```
#### Setup relationship
```php
<?php
// In Post.php
public function tags()
{
    $this->belongsToMany(Tag::class);
}
// $post = Post::find(1);
// $post->tag will list out all tags belonging to the post

// In Tag.php
public function posts()
{
    return $this->belongsToMany(Post::class);
}

```
## Add tag to post
```php
<?php
// Assign Tags to a post
$post = Post::first();
$tag = Tag::where('name', 'gaming')->first();
$post->tags()-> attach($tag);

// To get all posts with tags use the Laravel helper instead of filtering through each one
Post::with('tags')->get();
```
#### Tags route
```php
<?php
// Setup Routes
Route::get('/posts/tags/{tag}', 'TagsController@index')
// Create TagsController
public function index(Tag $tag)
{
    $posts = $tag->posts;
    return view('posts.index', compact('posts'));
}
```
## Get Tag by name
Instead of fetching tags by id `/posts/tags/1`. We can fetch tags by its name `/posts/tags/gaming`.
```php
<?php
// In Tag.php
public function getRouteKeyName()
{
    return 'name';
}
```
## Show tags on sidebar
```php
<?php
// In AppServiceProvider, add a new view composer
$view->with("tags" App\Tag::pluck('name')); // Get only the name of all tags

// Show in sidebar
@foreach ($tags as $tag)
    <li><a href="/posts/tags/{{ $tag }}">{{ $tag }}</a></li>
@endforeach
```
#### Only show tags if it has posts
```php
<?php
App\Tag::has('posts')->pluck('name');

// Clean up view composer
$archives = \App\Post::archives();
$tags = \App\Tag::has('posts')->pluck('name');

$view->with(compact('archives', 'tags'));
```
