---
title: Ruby basics
---

<!-- MarkdownTOC -->

* [Hash](#hash)
* [Blocks](#blocks)
* [Function](#function)
* [Class](#class)

<!-- /MarkdownTOC -->

# Hash
Hash
:  An unordered collection of data, where the data is organized into "key/value pairs"

```ruby
# To create a hash:
books = {key: value}

# To add a key with value:
books["Mistborn"] = :splendid
books["Steelheart"] = :splendid
books["Kingkiller"] = :good
# => {"Mistborn" => :spendid, "Steelheart" => :splendid, "Kingkiller" => :good}

# To view value from key:
books["Mistborn"] # => splendid
# To view all keys
books.keys
```
#### Symbol
Use `:` before strings to create a ~~symbol~~. Ex. :splendid, :good...
A ~~symbol~~ in Ruby uses *less computer memory*. If you need to use a word over and over again, use a symbol. The computer will store a symbol only once.

#### Loop through hash of hashes
```ruby
books_hash = {
  "Mistborn" => {
    :author => "Brandon Sanderson",
    :publisher => "Tor Books",
    :ratings => 4.42
  },
  "Kingkiller" => {
    :author => "Patrick Rothfuss",
    :publisher => "Penguin Group",
    :ratings => 4.55
  },
  "Steelheart" => {
    :author => "Brandon Sanderson",
    :publisher => "Tor Books",
    :ratings => 4.22
  }
}
book_ratings = {}
books_hash.each do |book_name, book_details|
  book_ratings[book_name] = book_details[:ratings]
end
puts book_ratings
```
```ruby
{"Mistborn"=>4.42, "Kingkiller"=>4.55, "Steelheart"=>4.22}
```
# Blocks
Blocks are always attached to methods. Blocks are chunks of code that can be tacked on to methods.

```ruby
# Create new empty hash with initial ratings of 0
ratings = Hash.new(0)
# Go through each value of books, assign it the variable "rate", and add 1 each time it matches.
books.values.each {|rate| ratings[rate] += 1}
puts ratings

# => {:spendid => 2, :good => 1}
```
# Function
```ruby
# Basic function syntax
def water_status(minutes)  
  if minutes < 7  
    puts "The water is not boiling yet."  
  elsif minutes == 7  
    puts "It's just barely boiling"  
  elsif minutes == 8  
    puts "It's boiling!"  
  else  
    puts "Hot! Hot! Hot!"  
  end  
end

water_status(3)
# => The water is not boiling yet
water_status(10)
# => Hot! Hot! Hot!
```
# Class
#### Class basic syntax
```ruby
# Defines attributes and methods of Student
class Student
  attr_accessor :first_name, :last_name, :primary_phone_number

  def introduction
    puts "Hi, I'm #{first_name}!"
  end

  def favorite_number
    7
  end
end

# Creates instance of Student
frank = Student.new
frank.first_name = "Frank"
frank.introduction
# => Hi, I'm Frank!
```

Instance variable
:  An instance variable is an object's variable; it will last as long as the object does. They have `@` in front of their names.

```ruby
class Die
  def initialize # initializes Die object right when it's created (die will roll on creation)  
    roll   
  end
  def roll
    @numberShowing = 1 + rand(6)   
  end
  def showing
    @numberShowing   
  end
end

puts Die.new.showing
# => 3
```
