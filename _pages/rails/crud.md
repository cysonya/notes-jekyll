---
title: CRUD
---
<!-- MarkdownTOC -->

* [CRUD Overview](#crud-overview)
* [Create](#create)
* [Read](#read)
* [Update](#update)
* [Delete](#delete)

<!-- /MarkdownTOC -->

# CRUD Overview
Source: [Rails for Zombies](http://railsforzombies.org/)

CRUD
:  stands for Create, Read, Update, Delete. Every data storage **must have** this feature.

![crud](./images/crud.png)

# Create
Syntax to create table and add data:
![create_table](./images/create_table.png)

*No need to set ID as Rails does this for you*
# Read
```ruby
Tweet.find(2) # Returns a single tweet with id of 2
Tweet.find(3, 4, 5) # Returns an array of tweets, id of 3, 4 or 5
Tweet.first # Returns the first tweets
Tweet.last # Returns the last tweets
Tweet.all # Returns array of all tweets
Tweet.count # Returns total number of tweets
Tweet.order(:zombie) # Returns all tweets, ordered by zombies
Tweet.limit(10) # Returns the first 10 tweets
Tweet.where(zombie: "ash") # Returns all tweets from zombie named 'ash'
```
![method_chaining](./images/method_chaining.png)

# Update
![update](./images/update.png)

# Delete
![Delete](./images/Delete.png)
