---
title: MVC
---
<!-- MarkdownTOC -->

* [MVC Overview](#mvc-overview)
* [Model](#model)
  * [Relational database](#relational-database)
* [Views](#views)
  * [Inside html](#inside-html)
* [Controllers](#controllers)
  * [Inside controller](#inside-controller)
  * [Parameters](#parameters)
  * [Actions](#actions)
  * [Authentication](#authentication)
* [Routes](#routes)
  * [Custom routes](#custom-routes)
  * [Named routes](#named-routes)
  * [Redirect](#redirect)
  * [Root route](#root-route)
  * [Parameters](#parameters-1)

<!-- /MarkdownTOC -->

# MVC Overview
#### MVC Architecture
![mvc_architecture](./images/mvc_architecture.png)
# Model
Model
:  Model is our objects. it communicates with the database.
#### Define Tweet model
![def_tweet_model](./images/def_tweet_model.png)
When we write '`Tweet`' anywhere in the program, we are referencing the `Tweet` model. `Tweet` **inherits** from "*ActiveRecord::Base*" which maps the class to the table '`tweets`'.
When we call `Tweet.find(3)`, it ~~creates an instance of Tweet~~ with ID #3.

***Models in Rails use a singular name, and their corresponding database tables use a plural name.***

#### Validate Tweet
If we create a new Tweet without setting any values,
```ruby
t = Tweet.new
t.save
```
It'll add a row to table '`tweets`', with blank status and zombie. We don't want that, we want to validate it
![create_tweet](./images/create_tweet.png)

![validate_syntax](./images/validate_syntax.png)
## Relational database
![tweets_table](./images/tweets_table.png)![tweet_using_relationship](./images/tweet_using_relationship.png)

# Views
~~Views~~ is the visual representation of the application, the user interface.  
index.html.erb -> ~~erb~~ stands for embedded Ruby code inside HTML

The folder *views/layouts/application.html.erb* holds Headers, footers etc.. Templates; Every page we create uses this code by default.

## Inside html
#### To create a link
![links_routes](./images/links_routes.png)

```erb
<%= link_to('text to show', path, html_options ={}) %>  
```
#### Common url paths
![links_routes](./images/links_routes.png)

# Controllers
Controller
:  processes and responds to events, such as user actions. Uses `models` to get data out of database, uses` views` to display data that comes out of models.
![links_routes](./images/links_routes_lkgm273if.png)
When request comes into the application,  
1. it first hits a controller  
2. it looks inside controllers for a file called tweets_controller.rb   
3. then renders out show.html.erb in views.

## Inside controller
```ruby
# When /tweets/1 requests comes in, it'll call the 'show' method
def show
  @tweet = Tweet.find(1) # Return from the Tweet model with id 1
  # To change the default from rendering out 'show.html.erb':
  render action: 'status' # renders out 'status.html.erb'
end
```
`@` creates an ~~instance variable~~ that grants `view` access to the variable
## Parameters
#### Params Explained
Rails generate ~~params which are hashes~~ passed in by user's browser. The params come from the user's browser when they request the page. For an HTTP GET request, which is the most common, the params are encoded in the url.

For example, if a user's browser requested `http://www.example.com/?foo=1&boo=octopus` then `params[:foo]` would be "1" and `params[:boo]` would be "octopus".

In HTTP/HTML, the params are really just a series of key-value pairs where the key and the value are strings, but Ruby on Rails has a special syntax for making the params be a hash with hashes inside.

For example, if the user's browser requested `http://www.example.com/?vote[item_id]=1&vote[user_id]=2` then `params[:vote]` would be a hash, `params[:vote][:item_id]` would be "1" and `params[:vote][:user_id]` would be "2".
#### Access a hash
```ruby
# To get tweet from all IDs
def show
  @tweet = Tweet.find(params[:id])
end
# To access a hash
# ex. params= { status: "I'm dead" }
@tweet = Tweet.create(status: params[:status])
# Alternate syntax
@tweet = Tweet.create(params[:tweet])
# Often rails would have a hash within a hash
# /tweets?tweet[status]=I'm dead
@tweet = Tweet.create(status: params[:tweet][:status]) # To access value in status, we need to go through 2 keys to get "I'm dead"
```
#### Strong Parameters
Users can set any attributes in the URL that would go into our Params.
So we are required to use **Strong Parameters** – We need to specify the parameter key our action expects.
```ruby
# Require tweet key is sent in from our hash
require(:tweet)
# And the attributes we will permit to be set from this hash
permit(:status)
# Final version
@tweet = Tweet.create(params.require(:tweet).permit(:status))
# If there are multiple things we need to permit, we use an array:
params.require(:tweet).permit([:status, :location])
```

***Strong Parameters required ONLY when Creating or Updating with multiple attributes***
## Actions
#### Common controller actions
![controller_actions](./images/controller_actions.png)
#### Vaoid repetition
Some methods share the same code such as Edit, Update, Destroy -> They make you look up current ID of tweet to check for authorization. To get rid of repitition:

![controller_repetition](./images/controller_repetition.png)

## Authentication
Add authentication to prevent random people to edit your tweet:
![session](./images/session.png)![flash_redirect](./images/flash_redirect.png)

Session
:  Works like a per user hash. Every user gets their own session hash that we can store things in. When user logs in, we can set their session hash that has their ID in it.

flash[:notice]
:  Rails helper hashes we can use to send messages back to user.

redirect_to(path)
:  Allows you to redirect the browser to a different action.

# Routes
In order to properly find paths and have them route into actions in controller,
We need to define routes inside our Router, *config/routes.rb*:
```ruby
ZombieTwitter::Application.routes.draw do
  resources :tweets
end
```
Rails provides a `resources` method which can be used to declare a standard REST resource. A resource is the term used for a collection of similar objects, such as articles, people or animals. You can create, read, update and destroy items for a resource and these operations are referred to as CRUD operations.

Resource creates URL paths for CRUD operations. Ex. /edit, /new

## Custom routes
Redirect /tweet/new to /new_tweet

![redirect_new](./images/redirect_new.png)
## Named routes
![name_routes](./images/name_routes.png)
## Redirect
![redirect](./images/redirect.png)
## Root route

```ruby
root to: "tweets#index" # is controller, index is the action
# Then in view we can link to root with:
link_to("All Tweets", root_path)
```
## Parameters
#### Allow url get listing of tweets
![route_index](./images/route_index.png)

#### To make route, work in routing file
![route_file](./images/route_file.png)

#### Show all tweets of user
![tweet_user](./images/tweet_user.png)
