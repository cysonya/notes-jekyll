---
title: Blogger
---
<!-- MarkdownTOC -->

* [Generate Article Model and Controller](#generate-article-model-and-controller)
* [Show all Articles](#show-all-articles)
* [Show single article](#show-single-article)
* [Create new article](#create-new-article)
  * [Add `new` action](#add-new-action)
  * [The `Create` action](#the-create-action)
* [Delete Articles](#delete-articles)
* [Edit Article](#edit-article)
  * [Add `edit` action](#add-edit-action)
  * [The `Update` Action](#the-update-action)
  * [Create partial for form](#create-partial-for-form)
* [Add a flash message](#add-a-flash-message)
* [Generate Comments Model](#generate-comments-model)
* [Add comments in console](#add-comments-in-console)
* [Display comments of an article](#display-comments-of-an-article)
* [Add Comment form](#add-comment-form)
* [Generate Comments Controller](#generate-comments-controller)
  * [Additional comment features](#additional-comment-features)
* [Tags Relationship](#tags-relationship)
* [Generate Tag Model](#generate-tag-model)
* [Add Tagging interface](#add-tagging-interface)
* [Generate Tag List](#generate-tag-list)
* [Show Tags in article](#show-tags-in-article)
* [Show articles by tags](#show-articles-by-tags)

<!-- /MarkdownTOC -->


Source: [Blogger Tutorial](http://tutorials.jumpstartlab.com/projects/blogger.html)
# Generate Article Model and Controller
```ruby
bin/rails generate model Article
```
Runs the generate script to create a model called `Article`. It will generate a file that creates an `articles` model.
#### Create fields the Article database needs to have
A basic article has a title and body. In *db/migrate/(some_time_stamp)_create_articles.rb*:
```ruby
def change  
  create_table :articles do |t|  
    t.string :title  
    t.text :body  
    t.timestamps  
  end  
end
```

```ruby
bin/rake db:migrate
```
We tell `rake` to `db:migrate` which means "look in your set of functions for the database (`db`) and run the `migrate` function." The `migrate` action finds all migrations in the *db/migrate/folder*, looks at a special table in the DB to determine which migrations have and have not been run yet, then runs any migration that hasn’t been run.

#### Working with model in console
Initialize new record, set title and body:
```ruby
a = Article.new  
a.title = "Sample Article Title"  
a.body = "This is the text for my article, woo hoo!"  
a.save # saves to database  
Article.all # show all articles
```
Generates *articles_controller.rb* file with` ArticlesController` class that inherits from `ActionController:Base`
```ruby
bin/rails generate controller articles # note: plural and lowercase for controller
```
# Show all Articles
#### Create index action
When requesting */articles* in browser, router will try to call the index action inside `articles_controller`. So we need to define the `index` method:
```ruby
def index  
  @articles = Article.all  
end
```
#### Create the Template
In *app/views/articles* create file named *index.html.erb*
```erb
<h1>All Articles</h1>  
<ul id="articles">  
  <% @articles.each do |article| %>  
    <li> <%= article.title %> </li>  
  <% end %>  
</ul>
```
Use `link_to` helper to view individual articles. In console, use `bin/rake routes` to view link paths.
```erb
<%= link_to(article.title, article_path(article)) %>
<!-- Create NEW article link -->
<%= link_to("Create new article", new_article_path) %>
```
# Show single article
#### Create show action
Within the controller, we have access to a method named `params` which returns us a hash of the request parameters. Often we’ll refer to it as "the `params` hash", but technically it’s "the `params` method which returns a hash".

Within that hash we can find the `:id` from the URL by accessing the key `params[:id]`. Use this inside the `show` method of `ArticlesController` along with the class method find on the Article class:

```ruby
def show  
  @article = Article.find(params[:id])
end
```
#### Create show template
Add file *show.html.erb* in *app/views/articles/*
```erb
<h1><%= @article.title %></h1>  
<p><%= @article.body %></p>  
<%= link_to "<< Back to Articles List", articles_path %>
```
# Create new article
To create a new article we need to add a `new` action and a `create` action. The `new` action uses **HTTP GET** protocol to render a form. A `GET` request isn't suppose to modify any data; you use a `GET` to serve the form version which creates a new unsaved record.

The `create` action uses **HTTP POST**. A `POST` is used for generating new data. It takes the record created from the `new` action and passes it to the `create` action, which then attempts to save it to the database.

## Add `new` action
Inside *articles_controller*:
```ruby
def new  
  @article = Article.new  
end
```
`Article.new` creates blank object so it can be called by `form_for `

#### Create form in view template
Add file *new.html.erb* in *app/views/articles/*
```erb
<h1>Create a New Article</h1>
<%= form_for(@article) do |f| %>  
  <ul>  
    <% @article.errors.full_messages.each do |error| %>  
      <li><%= error %></li>  
    <% end %>  
  </ul>  
  <p> <%= f.label :title %><br />
  <%= f.text_field :title %> </p>
  <p> <%= f.label :body %><br />
  <%= f.text_area :body %> </p>  
  <p> <%= f.submit %> </p>  
<% end %>
```
- `form_for` is a Rails helper method which takes one parameter, in this case `@article` and a block with the form fields. The first line basically says "Create a form for the object named `@article`, refer to the form by the name `f` and add the following elements to the form…"
- The `f.label` helper creates an HTML label for a field. This is good usability practice and will have some other benefits for us later
- The `f.text_field` helper creates a single-line text box named title
- The `f.text_area` helper creates a multi-line text box named body
- The `f.submit` helper creates a button labeled "Create"

## The `Create` action
#### Pull out form data
Before we can send the client to the `show` method, we need to process the data. The data from the form is accessible through the `params` method.

```ruby
def create  
  @article = Article.new(params[:article])  
  @article.save  
  redirect_to article_path(@article)  
end
```
`params[:article]` returns a hash that contains `params[:article][:title]` and `params[:article][:body]`.  
For security reasons, it’s not a good idea to blindly save parameters sent into us via the params hash. Luckily, Rails gives us a feature to deal with this situation: Strong Parameters.

It works like this: You use two new methods, `require` and `permit`. They help you declare which attributes you’d like to accept. Most of the time, they’re used in a helper method. Add the below code to *app/helpers/articles_helper.rb*:
```ruby
module ArticlesHelper  
  def article_params  
    params.require(:article).permit(:title, :body)  
  end  
end
```
Then in *articles_controller.rb** add:
```ruby
class ArticlesController < ApplicationController  
include ArticlesHelper  
  def create  
    @article = Article.new(article_params)  
    @article.save  
    redirect_to article_path(@article)
  end
end
```

# Delete Articles
Add delete button to *show.html.erb* template
```erb
<%= link_to "delete", article_path(@article), method: :delete %>
```
Most browsers support all four verbs, `GET`, `PUT`, `POST`, and `DELETE`, HTML links are always `GET`, and HTML forms only support `GET` and `POST`. So what are we to do?
Rails’ solution to this problem is to fake a `DELETE` verb. Through some JavaScript tricks, Rails can now pretend that clicking this link triggers a `DELETE`.
#### Add destroy action
In *articles_controler.rb*:
```ruby
def destroy  
  @article = Article.find(params[:id]) # Use `params[:id]` to find the article in the database
  @article.destroy # Call `.destroy` on that object
  redirect_to article_path # Redirect to the articles index page
end
```
#### Add confirmation to delete
Add the following to the delete button in *show.html.erb*
```erb
<!-- This will pop up an alert box when link is clicked -->
data: {confirm: "Really delete the article?"}
```

# Edit Article
Just like the `new` action sends its form data to the create action. The edit action sends its form data to the `update` action.
## Add `edit` action
```ruby
# The edit action finds the object and displays the form
def edit  
  @article = Article.find(params[:id])  
end
```
#### Add edit link
In *show.html.erb*:
```erb
<%= link_to "edit", edit_article_path(@article) %>
```

#### Create edit form
In *app/views/articles/*, create a file called *edit.html.erb*
```erb
<h1>Edit an Article</h1>  
<%= form_for(@article) do |f| %>  
  <ul>  
  <% @article.errors.full_messages.each do |error| %>  
    <li><%= error %></li>  
  <% end %>  
  </ul>  
  <p> <%= f.label :title %><br />
  <%= f.text_field :title %> </p>
  <p> <%= f.label :body %><br />
  <%= f.text_area :body %> </p>  

  <p> <%= f.submit %> </p>  
<% end %>
```
## The `Update` Action
Within our *articles_controller.rb*, the update method will look very similar to create:
```ruby
def update  
  @article = Article.find(params[:id])  
  @article.update(article_params)  
  redirect_to article_path(@article)  
end
```
The only new bit here is the `update` method. It’s very similar to `Article.new` where you can pass in the hash of form data. It changes the values in the object to match the values submitted with the form. One difference from `new` is that `update` automatically saves the changes.
We use the same `article_params` method as before so that we only update the attributes we’re allowed to.

## Create partial for form
The `edit` and `new` form are basically the same. We can abstract this into a single file called a ~~partial~~. Then reference this partial from both *new.html.erb* and *edit.html.erb*.

1. Create file *app/views/articles/_form.html.erb*
2. Paste the form in that file

Render the partial:
```erb
<%= render partial: 'form' %>
```
# Add a flash message
We can add a flash message to tell the user their action was successful. In `update` method, add:
```ruby
flash.notice = "Article '#{@article.title}' Updated!"
```
#### Add flash notice to layout
The update method redirects to the `show`. Since we use the flash in many actions of the application, it is preferred to add it to layout. The file *app/views/layouts/application.html.erb* is a layout used to wrap multiple view templates in your application. You can create layout specific to controller.

In the layout, the `yield` is where the view template content will be injected. Just above that, we can add our flash message:
```erb
<p class="flash"><%= flash.notice %></p>
```
# Generate Comments Model
What kind of data isa  comment?
- It's attached to an article
- It has an author name
- It has a body
```ruby
$bin/rails generate model Comment author_name: string body:text article:references
$bin/rake db:migrate
```
#### Relationships
The power of SQL databases is the ability to express relationships between elements of data. We can join together the information about an order with the information about a customer. Or in our case here, join together an article in the `articles` table with its `comments` in the comments table. We do this by using foreign keys.

Foreign keys are a way of **marking one-to-one and one-to-many** relationships. An article might have zero, five, or one hundred comments. But **a comment only belongs to one article**. These objects have a one-to-many relationship – **one article connects to many comments**.

Part of the big deal with Rails is that it makes working with these relationships very easy. When we created the migration for comments we started with an `references` field named `article`. The Rails convention for a one-to-many relationship:
- the objects on the "many" end should have a foreign key referencing the "one" object.
- that foreign key should be titled with the name of the "one" object, then an underscore, then "id".
In this case one article has many comments, so each comment has a field named `article_id`.

In *app/models/comment.rb*
```ruby
class Comment < ActiveRecord::Base
  belongs_to :article
end
```

A comment relates to a single article, it "~~belongs to~~" an article. We then want to declare the other side of the relationship inside *app/models/article.rb* like this:
```ruby
class Article < ActiveRecord::Base
  has_many :comments
end
```
Now an article "has many" comments, and a comment "belongs to" an article.

# Add comments in console
```ruby
a = Article.first
c = a.comments.new
c.author_name = "Daffy Duck"
c.body = "I think this article is thhh-thhh-thupid!"
c.save

d = a.comments.create(author_name: "Chewbacca", body: "RAWR!")
```
For the first comment, `c`, I used a series of commands like we’ve done before. For the second comment, `d`, I used the `create` method. `new` doesn’t send the data to the database until you call `save`. With create you build and save to the database all in one step.

# Display comments of an article
In *app/views/articles/show.html.erb*:
```erb
<h3>Comments</h3>  
<%= render partial: 'articles/comment', collection: @article.comments %>
```
This renders a partial named `comment` and that we want to do it once for each element in the collection `@article.comments`. We saw in the console that when we call the `.comments` method on an article we’ll get back an array of its associated comment objects. This render line will pass each element of that array one at a time into the partial named `comment`.

Create *app/views/articles/_comment.html.erb*
```erb
<div>  
  <h4>Comment by <%= comment.author_name %></h4>  
  <p class="comment"><%= comment.body %></p>  
</div>
```
# Add Comment form
In *show.html.erb*, render comment form partial:
```erb
<%= render partial: 'comments/form' %>
```
First look in your `articles_controller.rb` for the `new` method.
Remember how we created a blank `Article` object so Rails could figure out which fields an article has? We need to do the same thing before we create a form for the `Comment`.
But when we view the article and display the comment form we’re not running the article’s `new` method, we’re running the show method like this:
```ruby
@comment = Comment.new  
@comment.article_id = @article.id
```
Inside *comments/_form.html.erb* partial:
```erb
<h3>Post a Comment</h3>  
<%= form_for [ @article, @comment ] do |f| %>  
  <p>  
    <%= f.label :author_name %><br/>  
    <%= f.text_field :author_name %>  
  </p>  
  <p>  
    <%= f.label :body %><br/>  
    <%= f.text_area :body %>  
  </p>  
  <p> <%= f.submit 'Submit' %> </p>  
<% end %>
```
The `form_for` helper is trying to build the form so that it submits to `article_comments_path`. That’s a helper which we expect to be created by the router, but we haven’t told the router anything about Comments yet. Open *config/routes.rb* and update your article to specify comments as a sub-resource.
```ruby
resources :articles do  
  resources :comments  
end
```
# Generate Comments Controller
```ruby
$bin/rails generate controller comments
```
Add `create` action inside *comments_controller.rb*:
```ruby
def create  
  @comment = Comment.new(comment_params)  
  @comment.article_id = params[:article_id]  

  @comment.save  
  redirect_to article_path(@comment.article)  
end  
def comment_params  
  params.require(:comment).permit(:author_name, :body)  
end
```
## Additional comment features
#### Add comment count
In *article/views/show.html.erb*:
```erb
<h3>Comments (<%= @article.comments.size %>)</h3>
```
#### Show relative time
In *_comment.html.erb*
```erb
<p>Posted <%= distance_of_time_in_words(comment.article.created_at, comment.created_at) %> ago</p>
```
# Tags Relationship
#### Relationship
Articles can have many tags. A tag can relate to many articles. So, articles and tags have a ~~many-to-many~~ relationship.

Many-to-many relationships are tricky because we’re using an SQL database. If an `Article` "has many" `tags`, then we would put the foreign key `article_id` inside the `tags` table - so then a `Tag` would "*belong to*" an `Article`. But **a tag can connect to many articles**, not just one. We can’t model this relationship with just the `articles` and `tags` tables.

When we start thinking about the database modeling, there are a few ways to achieve this setup. One way is to create a "join table" that just tracks which tags are connected to which articles. Traditionally this table would be named `articles_tags` and Rails would express the relationships by saying that the Article model `has_and_belongs_to_many Tags`, while the `Tag` model `has_and_belongs_to_many Articles`.

Most of the time this isn’t the best way to really model the relationship. The connection between the two models usually has value of its own, so we should promote it to a real model. For our purposes, we’ll introduce a model named "`Tagging`" which is the connection between `Articles` and `Tags`.

#### The relationships will setup like this
- An Article `has_many` Taggings
- A Tag `has_many` Taggings
- A Tagging `belongs_to` an Article and `belongs_to` a Tag

# Generate Tag Model
With tag and article relationships in mind, let’s design the new models:
- Tag
  - `name`: A String
- Tag
  - `tag_id`: Integer holding the foreign key of the referenced Tag
  -  `article_id`: Integer holding the foreign key of the referenced Article
In *app/models/article.rb* and *app/models/tag.rb*:
```ruby
has_many :Taggings
```
This type of relationship is very common. In practical usage, if I had an object named article and I wanted to find its Tags, I’d have to run code like this:
```ruby
tags = article.taggings.collect{|tagging| tagging.tag}
```
In Rails we can express this "`has many`" relationship through an existing "`has many`" relationship. We will update our `article` model and `tag` model to express that relationship.
```ruby
# In app/models/article.rb:
has_many :taggings  
has_many :tags, through: :taggings  
# In app/models/tag.rb:
has_many :taggings  
has_many :articles, through: :taggings
```
Now if we have an object like `article` we can just ask for `article.tags` or, conversely, if we have an object named `tag` we can ask for `tag.articles`.

#### Create tags in console
```ruby
a = Article.first
a.tags.create name: "tag1"
a.tags.create name: "tag2"
a.tags
#=> [#<Tag id: 1, name: "tag1", created_at: "2016-...
```
# Add Tagging interface
When I write an article, I want to have a text box where I can enter a list of zero or more tags separated by commas. When I save the article, my app should associate my article with the tags with those names, creating them if necessary.
```erb
<!-- Add the following to app/views/articles/_form.html.erb: -->
<p>  
  <%= f.label :tag_list %><br />  
  <%= f.text_field :tag_list %>  
</p>
```
```ruby
# In article_controller.rb:
def tag_list  
  self.tags.collect do |tag|  
    tag.name  
  end.join(", ")  
end
```
Then we need to permit `tag_list` in `article_params`. In *app/helpers/articles_helper.rb*:
```ruby
def article_params  
  params.require(:article).permit(:title, :body, :tag_list)  
end
```
When we hit save, it goes to the `create` method. Since the `create` method passes all the parameters from the form into the `Article.new` method, the tags are sent in as the string "technology, ruby". The new method will try to set the new Article’s `tag_list` equal to "technology, ruby" but that method doesn’t exist because there is no attribute named `tag_list`.
We can define the `tag_list=` method inside *article.rb*:
```ruby
def tag_list=(tags_string)  

end
```
# Generate Tag List
The `Article#tag_list=` method accepts a parameter, a string like "tag1, tag2, tag3" and we need to associate the article with tags that have those names. The pseudo-code would look like this:
- Split the tags_string into an array of strings with leading and trailing whitespace removed (so "tag1, tag2, tag3" would become ["tag1","tag2","tag3"]
- For each of those strings…
  - Ensure each one of these strings are unique
  - Look for a Tag object with that name. If there isn’t one, create it.
  - Add the tag object to a list of tags for the article
- Set the article’s tags to the list of tags that we have found and/or created.

#### Split tags_strings
```ruby
tag_names = tags_string.split(",").collect{|s| s.strip.downcase}.uniq
```
- The `String#split(",")` will create the array  
- `Array#collect` will take each element of that array and return array
- `String#strip` will remove white spaces
- `String#downcase` will set string to lowercase
- `Array#uniq` removes duplicate items from an array.
#### Create or find tag
```ruby
new_or_found_tags = tag_names.collect { |name| Tag.find_or_create_by(name: name) }
```
#### Final version
```ruby
def tag_list=(tags_string)  
  tag_names = tags_string.split(",").collect{|s| s.strip.downcase}.uniq  
  new_or_found_tags = tag_names.collect { |name| Tag.find_or_create_by(name: name) }  

  self.tags = new_or_found_tags  
end
```
# Show Tags in article

```erb
<!-- In app/views/articles/show.html.erb -->
<p>  
  Tags:  
  <% @article.tags.each do |tag| %>  
    <%= link_to tag.name, tag_path(tag) %>  
  <% end %>  
</p>
```

```ruby
# Create tag_controller
$ bin/rake generate controller tags

# Add tags as a resource to config/routes.rb:
resources :tags
 ```
# Show articles by tags
```ruby
# In tags_controller.rb:
def show  
  @tag = Tag.find(params[:id])  
end
```
Then create show template *app/views/tags/show.html.erb*:
```erb
<h1>Articles Tagged with <%= @tag.name %></h1>  
<ul>  
  <% @tag.articles.each do |article| %>  
    <li><%= link_to article.title, article_path(article) %></li>
  <% end %>  
</ul>
```
