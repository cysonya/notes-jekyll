---
title: Stock Investing Basics
---
<!-- MarkdownTOC -->

* [Buffett's Process Overview](#buffetts-process-overview)
* [What is value investing?](#what-is-value-investing)
* [How to value a small business](#how-to-value-a-small-business)
* [What is a balance sheet?](#what-is-a-balance-sheet)
* [What is a share?](#what-is-a-share)
* [Buffett's Valuation Technique](#buffetts-valuation-technique)
	* [Determine intrinsic value](#determine-intrinsic-value)

<!-- /MarkdownTOC -->

# Buffett's Process Overview
Source: [How to invest in stocks](http://buffettsbooks.com/how-to-invest-in-stocks.html)
###### Buffett's 1st Rule
Find stock/business that **has great management**. Good management will make the business more likely to succeed and profit.
###### Buffett's 2nd Rule
**Find a business/stock you can own forever.** A stock is nothing more than owning a business. The essence of a stock is – a real business. You wouldn't want to buy a business and sell it the next day. You want to collect all the profits the business is making.
###### Buffett's 3rd Rule
**Find stocks that are stable and understandable**. This way you can assess the value of a stock because you can somewhat predict their cash flow and earnings power. When a company makes similar profits month to month, you can access how much money it will make in the long-run, and properly assess a price or value. 
###### Buffett's 4th Rule
**A stock must be undervalued**. So we need to determine a price that you think the stock is worth.


# What is value investing?
Source: [What is value investing?](http://buffettsbooks.com/howtoinvestinstocks/course1/investing-for-beginners/what-is-value-investing.html)

Value investing
:   The investor buys assets that has a higher value than the price you buy them for. He holds on to an asset, and this asset increases in value over time even with no effort or time put in.  

Asset
:   Something you own that continues to put money IN your pocket. *Ex.* Apartment building, stocks, bonds.

Liability
:   Something you own that takes money **OUT** of your pocket. *Ex.* A car

Intrinsic Value
:   Actual value of a security (stock or bond) as opposed to the market price (or trading price of stock or bond). Good investors know how to determine an investment's intrinsic value.   

***It is important to buy assets below their intrinsic value.***

# How to value a small business
Source: [How to value small business](http://buffettsbooks.com/howtoinvestinstocks/course1/investing-for-beginners/value-a-business.html)

Net Income / Earnings
: **Total profit** of a business after **subtracting** all of its **costs and taxes**. The net income is found at the bottom of the financial document called the income statement (also known as *the bottom line*).

Net Income before taxes
: The net income before subtracting taxes.

Earnings
:  Earnings and net income are pretty much the same. They both represent the profit of business after subtracting costs and taxes. The minor difference is that Earnings refers to the amount of **profit produced during a specific period** (monthly, quarterly etc.).

Revenue
: The money a company receives from its customers.  

Cost of Revenue
: Expenses that are related to producing revenue for the company. Example, if you are selling ice cream, the cost of ingredients would be the 'cost of revenue'.

Dividend
: Money that goes towards the owner.

Equity
: Money that goes back into the business (an investment).

Board of Directors
: Represents shareholders who hold huge amount of shares, like 10 million shares. As a small shareholder you have a voting power which is delegated to the board of directors.
#### What's a company worth?
If net income of company is $20,000, how much would you pay to buy the company?

If you pay $400,000, you will make 5% of your money. (*$20,000 / $400,000 = 0.05 => 5%*)
If you pay $200,000, you will make 10% of your money.
If you pay $100,00, you will make 20% of your money.

**Point is =>** The business never changed. But as you go through each purchase price, your return is different.

$400,000 is 20x the earnings (*$400,000 / $20,000 = 20*)
$200,000 is 10x the earnings.
$100,00 is 5x the earnings.  

If you overpay for stock, the *%* return you can expect to get each year goes down significantly.  

***NOTE:*** The value of the company is what you'd be willing to pay for. If you're happy with an estimated return of 5%, then buy the company for $400,000. If not, then pay less. The value is a function of how much risk you are willing to assume and how much you think you'll get back each year.

Warren Buffet likes to find companies that trade for ***less than 15 times*** the earnings.

# What is a balance sheet?
Source: [What is a blance sheet](http://buffettsbooks.com/howtoinvestinstocks/course1/investing-for-beginners/what-is-balance-sheet.html)

A company has **three main financial statements**: The income statement, the balance sheet and the cash flow statement.

#### The Balance Sheet
One way to think about the balance sheet is to ask what would happen if the business liquidated itself. In that scenario, would the business sell all its assets and pay off all liabilities? The balance sheet tells you **how much the company owns and how much it owes**. The answer to this question is known as ~~equity~~. On a per share basis, equity is known as the ~~book value~~.

#### Determine value of business
When the owner wants to sell the business, one way to determine the value of the business is to look at the equity. The larger the difference between the market price and equity, the riskier the business gets for an investor. A wide difference where the market price is a lot higher than the equity is equivalent to a low margin of safety. **If the equity is very close to the market price of the business or even higher, there is on the other hand a high margin of safety.** We should look for high margin of safety investments.

Total Assets
: Total amount of assets owned by a company. Check balance sheet for this number.

Total Liabilities
: All liabilities owed by a company.

Equity
: Total assets minus total liabilities. If the company goes out of business, the equity would be the money left after the company collected all their payments and paid all their debts.

Book value
: Equity per share

Balance Sheet
: A financial statement that **displays a company's assets, liabilities, and equity for a specific period in time**. It determines the margin of safety that you would receive.

Margin of safety
: Refers to the amount of money an investor pays for a business above the company's equity.

The income statement
: used to find the company's profit. Has the total revenue and net income number.

Liquidation
: turning the entire company into cash, which means you're ending it or killing the business.

***Note:*** The **income statement** is used to determine the earnings of the business; it **indicates the % return** you would get. The **balance sheet** is used to determines the equity of the business; it **indicates margin of safety**.
# What is a share?
Source: [What is a share](http://buffettsbooks.com/howtoinvestinstocks/course1/investing-for-beginners/what-is-a-share.html)

A share is often referred to as a stock. It should be considered as part of a real business. That also means that if a business is valued at $100,000 and has 10,000 shares the value of each share is $10. In the stock market field, you often hear about shares outstanding. Shares outstanding are simply the number of shares a business is divided into.

#### Terms to differentiate business and a single share
Earnings or earnings per share (EPS)
: Net income of a single share. To calculate this number, divide the company's total net income by the common shares outstanding.

Common Shares outstanding
: Total number of shares a company has been divided into.

Book value
: Equity (assets minus liabilities) of a single share

Market Price
: Also known as the trading price, it represents the current price a share is trading for.

#### Example to evaluate a business on a per share basis
The business is valued at $100,000 (*the market price*). The net income of the business is $20,000. If you buy the business for $100,000, you would get a 20% return (*$20,000 /  $100,000 = 2*) each year. Now if we divde this business to 10,000 shares (*shares outstanding*). That means $10 per share (*10,000 / 100,000*).  The ~~EPS~~ (earnings per share) would be *$20,000 / 10,000* = $2. So for one share, the earnings is $2 (20% return).

The equity (*assets minus liabilities*) of the whole business is $7,000. On a per share basis, it's called the ~~book value~~. The book value of the business would be *$7,000/10,000* = 0.7, 70 cents.

We can see that on a per share basis. The expected return in one year is still the same thing, 20%. So when we evaluate one share (*stock*) of the business, it is the same as evaluating the business as a whole! Buying one share is no different than buying the entire business.
#### Price to Earnings Ratio (P/E)
A basic valuation technique for a single share. The ratio is a comparison between the price you pay to buy a stock and the profit you may receive from 1 share in 1 year.

***Example***  
If market price of a stock is $10 and earnings (or EPS) are $2.  
The P/E is: *$10/$2 = $5*. Which means that for every $5 spent buying a stock, I should receive 1 dollar in profit one year later as an owner.  

***Every time you look at just one share, you need to look at it as if you are valuing the entire business.*** If you buy one share from Apple, you might as well buy the entire thing because ***all the numbers are proportional***. That is something fundamental in value investing.

# Buffett's Valuation Technique
Source: [Warren Buffett Stock Basics](http://buffettsbooks.com/howtoinvestinstocks/course1/investing-for-beginners/warren-buffett-stock-basics.html)

Warren Buffet has 4 rules for investing in stocks:
- Stocks have to be understandable
- Stocks must have a long term prospect
- Stocks must be managed by diligent leaders
- Never buy over valued stocks

#### Stocks have to be understandable
A stable and understandable stock is something we can calculate. When a company is producing the same earnings year in year out, and is growing consistently while producing 10% a year. We can predict that company's worth next year and the following years.

#### Stocks must have a long term prospect
When we look at a company we need to ask, "40 years from now, does the world still need this?". This allows us to take a hold on the business, so that it grows without us having to pay any taxes on that growth.

#### Stocks must be managed by diligent leaders
This is harder to assess. The company has to be managed by individuals who manage debt well. Sta away from any company that has a lot of debt!

#### Never buy over valued stocks
We need to determine the intrinsic value of a stock. If the stock is worth $50, we should bargain for an even lower price like $40. Always find the intrinsic value.

We should never go "well rule 3 and 4 are met, so we'll buy this company.". We must ensure **all 4 rules are met**.

## Determine intrinsic value
Buffett had a quick way to find companies that were worth investing using these 3 terms: ~~market price, earnings, and book value~~.

We can look at earnings that tells our potential returns and combine it with the book value for the assessment of our margin of safety.

#### Let's start with earnings

Market Price: $10
Earnings per share: $2

**Potential Returns**
P/E = $10/$2 = 5
Return = 20% (*2/10 = 0.2*)
"For every 5 dollars I spend, I can expect 1 dollar in return after a year."

---

More sample numbers on other stocks:
P/E = *25*, Return = *4%* (~~1/25 = 0.04~~)
P/E = *20*, Return = *5%*
P/E = *15*, Return = *6.6%*
P/E = *10*, Return = *10%*
P/E = *5*, Return = *20%*

When P/E is low and return is high, usually that means the margin of safety is going to get worse.

#### Look at book value
P/BV ratio
:  This ratio represents **market price / book value**. This ratio is a comparison of what you might pay for a company compared to its book value if the company liquidated (or terminated). A P/BV = 1 means that every dollar you spend purchasing, the company also has 1 dollar of equity already in the business.

Market Price: $10
Book value $0.70

**Price to Book Value ratio**
P/BV = $10/$0.70 = 14.3
"Every 14.3 dollars paid for this company is 1 dollar in book value."

---
P/BV = *5*, Safety = *20%* (~~1/5 = 0.2~~)
P/BV = *3*, Safety = *33%*
P/BV = *2*, Safety = *50%*
P/BV = *1.5*, Safety = *67%*
P/BV = *1*, Safety = *100%*
P/BV = *.7*, Safety = *143%*

If P/BV = 1, it means if you spend $100,000 to buy a company, you will get $100,000 back right away if the business is liquidated. When P/BV is low, it usually means P/E is worse.

---
#### Quick Valuation Tool
Here's a quick way to determine whether we should look more into a company. Buffet likes to find companies with:
- **P/E less than 15**
- **P/BV less than 1.5**

~~15 * 1.5 = 22.5~~
Therefore, ***if P/E * P/BV < 22.5, this is a company we need to look at.*** This number incorporates all the variables we are concerned with, market price, earnings, book values.

---
The market is nothing more than a place you go, to buy or sell stocks.
> I buy on the assumption that they could close the market tomorrow and not reopen it for five years. - WB

When Buffett buys stocks, he plans to own that company forever. Because he is buying company that gives him a 10-15% return, and there's no reason to sell a company that gives you 10-15% return.

#### Importance of patience and individuality
Patience
:  Patience is truly a virtue. Take your time and don't try to get rich overnight. Never break your rules! You need to be the type of person that is happy with 10-15% return of your money.

Individuality
:  Think for yourself. Don't invest based on other people's opinions. Often times, if you're doing what everyone else is doing, you're probably doing it wrong.
