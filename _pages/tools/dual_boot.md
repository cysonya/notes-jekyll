---
title: Dual Boot
---
<!-- MarkdownTOC -->

* [Setup Dual Boot](#setup-dual-boot)
* [Remove Dual Boot](#remove-dual-boot)

<!-- /MarkdownTOC -->

# Setup Dual Boot
- Install [etcher.io](https://etcher.io) to setup Ubuntu disk image on USB
- [Setup Ubuntu with Win10](https://www.tecmint.com/install-ubuntu-16-04-alongside-with-windows-10-or-8-in-dual-boot/)


# Remove Dual Boot
- Delete Ubuntu partition in Win10
- [Remove Ubuntu from boot menu](https://askubuntu.com/questions/921046/how-to-remove-ubuntu-from-boot-menu-after-deleting-ubuntu-partition-in-windows)