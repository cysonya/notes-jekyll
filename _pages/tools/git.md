---
title: Git
---
<!-- MarkdownTOC -->

* [Git setup](#git-setup)
* [Git Commands](#git-commands)

<!-- /MarkdownTOC -->

# Git setup
1. Install [git for windows](https://git-for-windows.github.io/)
2. `sudo apt-get install git-core` for Ubuntu

```bash
git config --global user.name "cysonya"
git config --global user.email "cysonya@gmail.com"

git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.ci commit
git config --global alias.st status
```

# Git Commands
#### Reset HEAD
```bash
# Reset HEAD
git reflog
# See a list of every thing you've done in git, across all branches!
# each one has an index HEAD@{index}. find the one before you broke everything
git reset HEAD@{index}
```
#### Add to something already committed
```bash
# Add to something already committed
git add .
git comit --ammed
# follow prompts to change or keep commit message, now your last commit contains that change!
```
#### Accidentally commit to master meant for another branch
```bash
# create new branch from master
git branch other-branch-name
# remove the commit from master branch
git reset HEAD~ --HARD
git checkout other-branch-name
```
#### Commit to wrong branch
```bash
# undo the last commit, but leave the changes available
git reset HEAD~ --soft
git stash
# move to the correct branch
git checkout name-of-the-correct-branch
git stash pop
```
