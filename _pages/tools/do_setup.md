---
title: Digital Ocean Setup
---
<!-- MarkdownTOC -->

* [Server Setup](#server-setup)
* [LAMP setup](#lamp-setup)
* [Setup multiple domains](#setup-multiple-domains)
* [SFTP with Filezilla](#sftp-with-filezilla)

<!-- /MarkdownTOC -->

# Server Setup
- [Setup remote database](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-remote-database-to-optimize-site-performance-with-mysql)
- [Connect to droplet with ssh](https://www.digitalocean.com/community/tutorials/how-to-connect-to-your-droplet-with-ssh)
- [Create new user with ssh authentication](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-14-04)
- [Setup firewall](https://www.digitalocean.com/community/tutorials/additional-recommended-steps-for-new-ubuntu-14-04-servers)
- [Setup Git deploy](https://www.digitalocean.com/community/tutorials/how-to-set-up-automatic-deployment-with-git-with-a-vps#local-machine)
- [Laravel setup](https://devmarketer.io/learn/deploy-laravel-5-app-lemp-stack-ubuntu-nginx/)

# LAMP setup
- [Install LAMP stack](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-14-04)
- [Install phpmyadmin](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-phpmyadmin-on-ubuntu-14-04)

# Setup multiple domains
[Setup multiple domains](https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-ubuntu-14-04-lts)

# SFTP with Filezilla
- [Setup sftp with Filezilla](https://www.digitalocean.com/community/tutorials/how-to-use-filezilla-to-transfer-and-manage-files-securely-on-your-vps)
- [SSH to Wordpress](https://www.digitalocean.com/community/tutorials/how-to-configure-secure-updates-and-installations-in-wordpress-on-ubuntu)

If permission denied, in server console
```bash
sudo chown -R youruser:youruser /var/www
# That will give user write access to all directories/files in var/www

# For Wordpress updates
sudo
sudo chown -R wp-user:wp-user /var/www/aspirationclan.com/public_html
```
