---
title: Scope & Closures
---
<!-- MarkdownTOC -->

* [Scope](#scope)
* [The compiler](#the-compiler)
  * [Compiler example](#compiler-example)
  * [Compiler speak \(RHS / LHS\)](#compiler-speak-rhs--lhs)
  * [Compiling and execution](#compiling-and-execution)
* [Lexical Scope](#lexical-scope)
* [Function Expression](#function-expression)
* [Let keyword - Blocks as scope](#let-keyword---blocks-as-scope)
  * [The `let` keyword](#the-let-keyword)
* [Hoisting](#hoisting)
* [Closure](#closure)

<!-- /MarkdownTOC -->

# Scope
One of the most fundamental paradigms of nearly all programming languages is the ability to store values in variables, and later retrieve or modify those values. The ability to store and pull values out of variables is what gives a program *state*.

Scope
:  Defines a set of rules for storing variables in some locations and for finding those variables at a later time.

#### Where and how do these scope rules get set?
Engine
:  Responsible for start-to-finish compilation and execution of our JavaScript program.

Compiler
:  Handles all the dirty work of parsing and code-generation.

Scope
:  Collects and maintains a look-up list of all the declared identifiers (variables), and enforces a strict set of rules as to how these are accessible to currently executing code.

# The compiler
#### 3 steps of compilations
In traditional compiled-language process will undergo typically three steps before it is executed, roughly called "compilation":

1. ~~Tokenizing/Lexing~~: Breaking up a string of characters into meaningful (to the language) chunks, called ~~tokens~~. For instance, consider the program: `var a = 2;`. This program would likely be broken up into the following tokens: `var`, `a`, `=`, `2`, and `;`. Whitespace may or may not be persisted as a token, depending on whether it's meaningful or not.
***Note***: The difference between tokenizing and lexing is subtle and academic, but it centers on whether or not these tokens are identified in a stateless or stateful way. Put simply, if the tokenizer were to invoke stateful parsing rules to figure out whether `a` should be considered a distinct token or just part of another token, that would be lexing.

2. ~~Parsing~~: Taking a stream (array) of tokens and turning it into a tree of nested elements, which collectively represent the grammatical structure of the program. This tree is called an "AST" (Abstract Syntax Tree).
The tree for `var a = 2;` might start with a top-level node called `VariableDeclaration`, with a child node called `Identifier` (whose value is `a`), and another child called `AssignmentExpression` which itself has a child called `NumericLiteral` (whose value is `2`).

3. ~~Code-Generation~~: The process of taking an AST and turning it into executable code. This part varies greatly depending on the language, the platform it's targeting, etc.

For JavaScript, the compilation happens right before the code is executed, unlike most compiled language that have plenty of time to optimize during build steps.

## Compiler example
#### Consider the program `var a = 2`
The `Engine` sees **two** distinct statements. One which `Compiler` will handle during compilation, and one which `Engine` will handle during execution.

The first thing `Compiler` will do with this program is perform *lexing* to break it down into *tokens*, which it will then parse into a tree. When `Compiler` gets to code-generation, it will proceed as:

1. Encountering `var a`, `Compiler` asks `Scope` to see if a variable `a` already exists for that particular scope collection. If so, `Compiler` ignores this declaration and moves on. Otherwise, `Compiler` asks `Scope` to declare a new variable called `a` for that scope collection.
2. `Compiler` then produces code for `Engine` to later execute, to handle the `a = 2` assignment. The code `Engine` runs will first ask `Scope` if there is a variable called `a` accessible in the current scope collection. If so, `Engine` uses that variable. If not, `Engine` looks elsewhere (see nested `Scope` section below).

If `Engine` eventually finds a variable, it assigns the value `2` to it. If not, `Engine` will return an error!

#### To summarize
Two distinct actions are taken for a variable assignment: First, `Compiler` declares a variable (if not previously declared in the current scope), and second, when executing, `Engine` looks up the variable in `Scope` and assigns to it, if found.

## Compiler speak (RHS / LHS)
LHS and RHS
:  LHS stands for left hand side of assignemtn operation. RHS stands for right hand side.

LHS and RHS don't necessarily always mean "left/right side of th e= assignment operator". There are several other ways assignments happen. It is better to think that, **RHS means "go get the value of..."** And **LHS means "who's the target of the assignment"** or **"what am I assigning this to?"**

When a RHS look-up occurs and the variable is not found in the nested scopes. It will result in a ReferenceError being thrown by `Engine`.  
In contrast, if the `Engine` performs a LHS look-up and doesn't find it in the global scope and if it's not running in "strict mode", then the `global Scope` will create a new variable for you in the global scope, and hand it back to `Engine`.

## Compiling and execution
#### How JavaScript engine compiles this line of code:
```javascript
var foo = "bar";
function bar() {
  var foo = "baz";

  function baz(foo) {
    foo = "bam";
    bam = "yay";
  }
  baz();
}
bar();
foo; // "bar"
bam; // "yay"
```
#### On compiling
1. On line 1, there's a variable declaration for an identifier called `foo`.
2. And `Engine` says, Hey global scope, I have a declaration for a variable called `foo`.
3. And `Scope` will respond back, got it I've got them registered in the global scope now.
4. On line 3, I see a function declaration with an identifier called `bar`.
5. Hey global scope, I have a function called `bar`. I want to register him in the global scope.
6. Now that we see we are in a function, we know that's a new scope. So we will descend into that function, and compile the contents of that function
7. On line 4, I see a variable declaration called `foo`
8. Hey scope of `bar`, I have a declaration for a varaible called `foo`, register him in the scope of `bar`
9. On line 6 , a function called `baz` is declared in scope of `bar`
10. Hey scope of `bar`, I have a function declaration called `baz`, register him in scope of `bar`.  
11. We descend into the function `baz`
12. The named parameter `foo` will be treated like a local variable
13. Hey scope of `baz`, I've got a declaration for a variable called `foo`, I need you to register him in the scope of `baz`.
14. We don't see anymore declaration, so it's done compiling

#### On execution
In execution phase, there's no more `var` anymore because we handled the declaration in the compilation phase so he doesn't exist in the execution phase. What's left in line 1 is `foo = "bar"`.
1. Hey Global Scope, I've got an LHS reference for a variable called `foo`, ever heard of him?
2. Yes I've heard of him, and he's going to give us a reference back to that variable `foo`. Now assign "bar" to `foo` since it's an immediate value.
3. Line 3 – 11 don't exist anymore because they were compiled away. So we move to line 13, `bar()`;
4. On line 13, Hey global scope, I've got an RHS reference for a variable called `bar`.
5. It returns a function object. Now we will attempt to execute the function.
6. On line 4, hey scope of `bar`, I have an LHS reference called `foo`, ever heard of him?
7. Yes, and assigns "baz".
8. It ignores line 6-9 because it was compiled away. So we move to line 10, `baz()`;
9. Hey scope of `bar`, I have a RHS reference for variable called `baz`, ever heard of him?
10. Yes and returns function object. Now we will execute the function.
11. On line 6, hey scope of `baz`, I have a LHS reference with variable called `foo`, ever heard of him?
12. Yes, and assigns "bam"
13. Hey scope of `baz`, I have a LHS reference with variable called `bam`, ever heard of him?
14. Nope. So Engine moves out of scope of `baz` to `bar` Hey scope of `bar`, I have a LHS reference with variable called `bam`, heard of him?
15. Nope. And now it goes to `global scope`. Hey global scope, I have a LHS reference with variable called `bam`, heard of him?
16. Yes. I just created It for you.
17. On line 14, hey global scope I have a RHS reference with variable called `foo`, heard of him?
18. Yes, and returns "bar"
19. On line 15, hey global I have a RHS reference with variable called `bam`, heard of him?
20. Yes, created him just recently and returns "yay" (from line 8, `bam = "yay"`)
21. On line 16, hey global scope I have an RHS for variable called `baz`, ever heard of him?
22. Nope. Here's the difference:

***Note difference***
With an LHS reference that is unfulfilled, an undeclared declaration, in non-strict mode it auto created one for us at global scope.
With an RHS reference, it is not going to create a function out of thin air. So it will throw a reference error.

# Lexical Scope
Lexical scope
:  Scope that is *defined at lexing time* (first phase of compiling). Which means at the time that you wrote your code, and that code then got compiled, the decisions for how all the scoping were going to occur were made in stone at that point.  

![lexical_scope](./images/lexical_scope.png)

**Bubble 1** encompasses the `global scope`, and has just one identifier in it: `foo`.
**Bubble 2** encompasses the scope of `foo`, which includes the three identifiers: `a, bar and b`.
**Bubble 3** encompasses the scope of `bar`, and it includes just one identifier: `c`.

Scope bubbles are defined by where the blocks of scope are written, which one is nested inside the other. The bubble for `bar` is entirely contained within the bubble for `foo`, because (and only because) that's where we chose to define the `function bar`.
#### Cheating lexical scope with `eval()`
A way to cheat lexical scope is the ~~eval~~ keyword. It takes any given string and it treats the string contents as if that had been code that existed at that point in the compilation.

![eval_keyword](./images/eval_keyword.png)

When we compile this code, we see the function `foo`, defined from lines 3 to 6, we see that the function does not have a variable called `bar` (line 5). When I execute line 5, I would expect it to go out to the global scope and get the value `bar` in line 1. But because we choose to pass in a variable declaration in the form of a string, line 8, and `eval` will run it on line 4. It cheats it, it pretends that that line of code had existed at compile time.

So in a sense, what it does is it modifies the existing lexical scope of `foo` to add a new declaration to it at runtime.

# Function Expression
We can effectively "hide" any enclosed variable or function declaration from the outside scope by wrapping a function around it. For example:
```javascript
var a = 2;
function foo() {
  var a = 3;
  console.log(a); // 3
}  

foo();
console.log(a); // 2
```
While this technique "works", it is not ideal. There are a few problems this introduces. The first is that we have to declare a named-function `foo()`, which means that the identifier name foo itself "pollutes" the enclosing scope (global, in this case). We also have to **explicitly call the function by name `foo()`** so that the wrapped code executes.

It would be more ideal if the function didn't need a name (or rather, the name didn't pollute the enclosing scope), and that function could automatically be executed. Function expressions solves this problem.
#### Function example expression
```javascript
// Simple function expression
var a = 2;
(function foo(){ // start function expression with ()
  var a = 3;
  console.log( a ); // 3


})(); // execute with ();


console.log( a ); // 2
```
In the first snippet, the name `foo` is bound in the enclosing scope, and we call it directly with `foo()`. In the second snippet, the name `foo` is not bound in the enclosing scope, but instead is bound only inside of its own function.

We can **execute** a function expression by add another `()` **on the end**, like `(function foo(){ .. })()`. The first enclosing `( )` pair makes the function an expression, and the second `()` executes the function. This pattern is so common, the term for it is ~~IIFE~~, stands for ~~Immediately Invoked Function Expression~~.
We can pass in arguments to IIFE's:
```javascript
// IIFE example with arguments
var a = 2;
(function IIFE( global ){
  var a = 3;
  console.log( a ); // 3
  console.log( global.a ); // 2
})( window ); // <- argument
console.log( a ); // 2
```
We pass in the window object reference, but we name the parameter global, so that we have a clear stylistic delineation for global vs. Non-global references.

# Let keyword - Blocks as scope

Consider the following code
```javascript
for (var i=0; i<2; i++) {
  console.log(i);
}
console.log('outside', i);
// 0
// 1
// outside 2
```

While most other programming languages support **Block Scope**, JavaScript does not. Meaning `var i` is actually accessible outside of the for-loop. Even though our intent is most likely to use i only within the context of that for-loop.
## The `let` keyword
The `let` keyword is another way to declare variables like `var`. It attaches the variable declaration to the scope of whatever block it's contained in. In other words, `let` implicitly hijacks any block's scope for its variable declaration.

#### Use of block scoping
```javascript
function process(data) {  
  // do something interesting
}
var someReallyBigData = { .. };
process( someReallyBigData );

var btn = document.getElementById( "my_button" );
btn.addEventListener( "click", function click(evt){
  console.log("button clicked");
});
```
The `click` function click handler callback doesn't need the `someReallyBigData` variable at all. That means, theoretically, `after process(..)` runs, the big memory-heavy data structure could be garbage collected. However, it's quite likely (though implementation dependent) that the JS engine will still have to keep the structure around, since the click function has a closure over the entire scope.

Block-scoping can address this concern, making it clearer to the engine that it does not need to keep someReallyBigData around:
```javascript
function process(data) {  
  // do something interesting
}
{
  let someReallyBigData = { .. };
  process( someReallyBigData );
}

var btn = document.getElementById( "my_button" );
btn.addEventListener( "click", function click(evt){
  console.log("button clicked");
});
```
Declaring explicit blocks for variables to locally bind to is a powerful tool that you can add to your code toolbox.

# Hoisting
```javascript
Consider this code:
a = 2;
var a;
console.log(a) // 2;
```
The output is 2, even though we would expect `undefined`, since the `var a` statement comes after the `a=2`. So why did it return 2? We need to look at how `Engine` compiles our code before it executes. **All declarations, both variables and functions, are processed first** before any part of your code is executed. So the code after compilation actually looks like this:
```javascript
// This is how the code is actually executed
var a;
a = 2;
console.log( a );
```
One way of thinking about this process is that **variable and function declarations are "moved"** from where they appear in the flow of the code **to the top of the code**. This behaviour is called ~~Hoisting~~.

***Note:*** Only declarations themselves are hoisted, while any assignments or executable logic are left in place. Function expressions are not hoisted either. Example:
```javascript
foo(); // TypeError
bar(); // ReferenceError
var foo = function bar() {
  // ...
};
// This snippet is executed (with hoisting) as:
var foo;
foo(); // TypeError
bar(); // ReferenceError
foo = function() {
  // ...
}
```
# Closure
Closure
:  Javascript functions are closures. Closure is when a function has access to variables outside its scope.

```javascript
var me = "Bruce Wayne"
function greetMe() {
  console.log('Hello, ' + me + '!')
}
greetMe()
// Hello, Bruce Wayne!
```
*Note:* `greetMe()` has access to the outer variable scope. It does not snapshot the value of `me`. 

```javascript
var me = "Bruce Wayne"
function greetMe() {
  console.log('Hello, ' + me + '!')
}
me = 'Batman'
greetMe()
// Hello, Batman!

```
#### A practical use case for closures

```javascript
function sendRequest() {
  var requestID = '123'
  $.ajax({
    url: '/myUrl',
    success: function(response) {
      alert('Request ' + requestID + ' returned')
    }
  });
}
```

Since all javascript functions are closures, including the callback function to `success:`. The callback function will have access to `requestID` declared outside of its scope even though it's executed way later.

[MDN article on closures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)

