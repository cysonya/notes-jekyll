---
title: Data Structure
---

<!-- MarkdownTOC -->

* [JS Vocabulary](#js-vocabulary)
* [Unary, Binary and Tenary Operators](#unary-binary-and-tenary-operators)
* [Array](#array)
* [Objects](#objects)
	* [Add object properties](#add-object-properties)
	* [Loop through object properties](#loop-through-object-properties)

<!-- /MarkdownTOC -->

# JS Vocabulary
Expression
:  Fragment of code that produces a value. *Ex.* `!false;`

Variables
:  Stores values

Keyword(reserved words)
:  Have special meaning, and cannot be used as variable names. *Ex.* `var`

Environment
:  Collection of variables and values that exists when a program starts up.

Function
:  Piece of program wrapped in a value. Executing a function is called ~~invoking, calling, or applying~~.

Arguments
:  Values given to call a function. *Ex* `foo("hello world");`

Return values
:  Values produced by function.

# Unary, Binary and Tenary Operators
Unary Operatro
:  Takes only one value.

```javascript
// !, typeof, - are examples of unary operator
console . log ( typeof 4.5 )  
// → number  
console . log ( typeof "x" )  
// → string
```
Binary Operator
:  Takes two values. *Ex.* (`&&, ||, !=`) are logical operators, and (`+, -, >`) etc...

Tenary Operator
:  Takes three values

```javascript
console . log ( true ? 1 : 2 );  
// → 1  
console . log ( false ? 1 : 2 );
// → 2
```
# Array
Array stores sequnces of values. It is written as a list.
```javascript
var listOfNumbers = [2, 3, 5, 7, 11];
console.log(listOfNumbers[1]);
// -> 3
```
#### Useful array methods
```javascript
// push() adds elements to end of array
var numbers = [1, 2, 3];
numbers.push(4); // [1, 2, 3, 4]

// pop() removes last element from array and returns that element
var myFish = ['angel', 'clown', 'mandarin', 'sturgeon'];
var popped = myFish.pop();
console.log(myFish); // ['angel', 'clown', 'mandarin' ]
console.log(popped); // 'sturgeon'

// shift() removes first item from list and returns that element
var a = [1, 2, 3];
a.shift(); // [2, 3]

// unshift() adds one or more elements to the beginning of an array
var a = [1, 2, 3];
a.unshift(4, 5); // [4, 5, 1, 2, 3]

// indexOf(searchValue[, fromIndex]) returns the index of the first occurence of the specified value, starting the search at fromIndex. Returns -1 if not found
'Blue Whale'.indexOf('Blue');     // returns  0
'Blue Whale'.indexOf('Blute');    // returns -1
'Blue Whale'.indexOf('Whale', 5); // returns  5

// lastIndexOf(searchValue[, fromIndex]) returns the index of the last occurence of the specified value, searching backwawrds from fromIndex.
'canal'.lastIndexOf('a');     // returns 3
'canal'.lastIndexOf('a', 2);  // returns 1
'canal'.lastIndexOf('a', 0);  // returns -1

// slice(begin, end)
var a = ['zero', 'one', 'two', 'three'];
var sliced = a.slice(1, 3); // ['one', 'two']

// concat() merges two or more arrays and returns a new array
var arr1 = ['a', 'b', 'c'];
var arr2 = ['d', 'e', 'f'];
var arr3 = arr1.concat(arr2);  // [ "a", "b", "c", "d", "e", "f" ]
```
# Objects
Arbitrary collections of properties. We can add or remove these properties as we please.
Object comes in the following syntax:
```javascript
var Objectname = { name: value};
// name:value is called the property of the object

var day1 = {
squirrel: false,
events: ["work", "touched tree", "pizza"]
};
console.log(day.squirrel); // -> false
```
#### Mutalibility
Immutable
:  An immutable object can't be changed after it's created. *Ex.* Numbers, strings and booleans

Mutable
:  A mutable object can be changed after it's created. *Ex.* object, array

```javascript
var object1 =  {value: 10};
var object2 = object1;  
var object3 = {value: 10};
console.log(object1 == object2); // -> true
console.log(object1 == object3); // -> false
```
***object1 != object 3 because objects are mutable***

## Add object properties
```javascript
var map = {};
function addToMap(item, value) {
map[item] = value
}
addToMap("Age", 31)
console.log(map["Age"]); // -> 31
console.log("Age" in map); // -> true
```
## Loop through object properties
```javascript
for (var item in map)
console.log("My " + item  + " is " + map[item]);
// -> My Age is 31
```
