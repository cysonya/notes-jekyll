---
title: this & Object Prototypes
---
<!-- MarkdownTOC -->

* [Simple rule of thumb](#simple-rule-of-thumb)
* [Call-site](#call-site)
* [The 4 `this` rules](#the-4-this-rules)
  * [Default Binding](#default-binding)
  * [Implicit Binding](#implicit-binding)
  * [Explicit Binding](#explicit-binding)
  * [Hard Binding](#hard-binding)
  * [`new` Binding](#new-binding)
* [Determining `this`](#determining-this)
* [`this` in ES6](#this-in-es6)

<!-- /MarkdownTOC -->

`this` references the **object** that is executing the current function.

# Simple rule of thumb
1. If the function is part of an object (a method), `this` references the object itself.
2. If the function is a regular function, NOT part of an object, `this` references the global object which is the `window` object in browsers.


Example of first rule:
```javascript
const video = {
  title: 'a',
  play() {
    console.log(this)
  }
}

video.stop = function() {
  console.log(this);
}
video.play() // {title: "a", play: function()...}
video.stop() // {title: "a", play: function()...}
```
Because `play()` and `stop()` are a method in the `video` object. `this` references the `video` object itself.

Example of the second rule:
```javascript
function playVideo() {
  console.log(this);
}

playVideo(); // Window {...}
```


# Call-site

`this` is neither a reference to the function itself, nor is it a reference to the function's lexical scope. `this` is actually a binding that is made when a function is invoked, and what it references is **determined entirely by the call-site** where the function is called.

Call-site
:  the location in code where a function is called/invoked (not where it's declared). 

Call-stack
:  the stack of functions that have been called to get us to the current moment in execution

Demonstration of call-stack and call-site:
```javascript
function baz() {
  // call-stack is: `baz`
  // so, our call-site is in the global scope

  console.log( "baz" );
  bar(); // <-- call-site for `bar`
}

function bar() {
  // call-stack is: `baz` -> `bar`
  // so, our call-site is in `baz`

  console.log( "bar" );
  foo(); // <-- call-site for `foo`
}

function foo() {
  // call-stack is: `baz` -> `bar` -> `foo`
  // so, our call-site is in `bar`

  console.log( "foo" );
}

baz(); // <-- call-site for `baz`
```


# The 4 `this` rules
We must inspect the call-site and determine which 4 rules apply.

## Default Binding
This is the most common case of function calls: standalone function invocation. Default binding refers to how `this` is the global context whenever a function is invoked when none of the other rules are applied.

```javascript
function foo() {
  console.log( this.a );
}

var a = 2;

foo(); // 2
```
`this.a` resolves to the global variable `a`. Why? Because the *default binding* for `this` applies to the function call, and so points `this` at the global object. 

How do we know that the *default binding* rule applies here? We examine the call-site to see how `foo()` is called. `foo()` is called with a plain, regular function reference. No other rules apply here, so the *default binding* rule gets applied.

## Implicit Binding
Implicit binding occurs when dot notation is used to invoke a function.

```javascript
function foo() {
  console.log( this.a );
}

var obj = {
  a: 2,
  foo: foo
};

obj.foo(); // 2
```

In implicit binding, **whatever is to the left of the dot** becomes the context for `this` in the function. In the example, because `obj` is the context for `this` in the `foo()` call, `this.a` is the same with `object.a`.

Only the top/last level of an object property reference chain matters to the call-site:
```javascript
function foo() {
  console.log( this.a );
}

var obj2 = {
  a: 42,
  foo: foo
};

var obj1 = {
  a: 2,
  obj2: obj2
};

obj1.obj2.foo(); // 42
```
#### Example between default binding and implicit binding
```javascript
var myMethod = function () {
  console.log(this);
};

var myObject = {
  myMethod: myMethod
};

myObject.myMethod() // this === myObject
myMethod() // this === window
```

## Explicit Binding
Explicit binding occurs when `.call()` or `apply()` are used on a function. 

```javascript
function foo() {
  console.log( this.a );
}

var obj = {
  a: 2
};

foo.call( obj ); // 2
```

**Notes:** `call()` and `apply()` are identical with respect to `this` binding.
#### Explicit binding takes precedence over implicit binding

```javascript
var myMethod = function () 
  console.log(this);
};

var obj1 = {
  a: 2,
  myMethod: myMethod
};

var obj2 = {
  a: 3,
  myMethod: myMethod
};

obj1.myMethod(); // 2
obj2.myMethod(); // 3

obj1.myMethod.call( obj2 ); // 3
obj2.myMethod.call( obj1 ); // 2
```

## Hard Binding
`.bind()` sets a `this` context and returns a **new function** of the same name with a bound `this` context. *Hard binding* is a form of *explicit binding*.

```javascript
var sayMyName = function () {
  console.log('My name is ' + this.name);
};

var jake = {
  name: 'Jake'
}

var sayMyName = sayMyName.bind(jake);
sayMyName(); // 'My name is Jake'
```

Hard binding takes precendence over explicit binding.
```javascript
var myMethod = function () {
  console.log(this);
};

var obj1 = {
  a: 2
};

var obj2 = {
  a: 3
};

myMethod = myMethod.bind(obj1); // 2
myMethod.call( obj2 ); // 2
```

## `new` Binding
The `new` operator creates a new empty object, `{}`, and sets `this` in the constructor function to point to the empty object.

```javascript
function Video(title) {
  this.title = title;
  console.log(this);
}
const v = new Video('a'); // Video {title: 'a'}
```
# Determining `this`

1. If function is called with `new` (*new binding*), then `this` is the newly constructed object
  - `var bar = new foo()`
2. If function is called with `call()` or `apply()` (*explicit binding*) or `bind()` (*hard binding*), then `this` is the explicitly specified object.
  - `var bar = foo.call(obj2)`
3. If function is called with a context (*implicit binding*), then `this` is that context object.
  - `var bar = obj1.foo()`
4. Otherwise, *default binding* of `this` is applied. If in `strict mod`, pick `undefined`, otherwise it'll be the `global/window` object.
  - `var bar = foo()`

# `this` in ES6
ES6 introduces a the arrow-function that does not follow the 4 rules. Arrow-functions use lexical scoping for `this` binding, which means they adopt the `this` binding from its enclosing function call.
