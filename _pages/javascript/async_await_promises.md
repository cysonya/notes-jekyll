---
title: Promises & Async/Await
---

<!-- MarkdownTOC -->

* [Promises](#promises)
  * [Promise Example](#promise-example)
* [Async/Await](#asyncawait)
  * [Description of `async/await`](#description-of-asyncawait)
  * [Benefit for async/await](#benefit-for-asyncawait)

<!-- /MarkdownTOC -->


# Promises
Callbacks is a way to tell the code that, when this thing is done execute this piece of code. Promises serve the same purpose as callbacks. Promises, like callbacks, are a way of dealing with things being asynchronous - when we don't know for certain in what order things will happen. 

```javascript
import loadImagePromised
from './load-image-promised'

loadImagePromised('images/cat3.jpg')
	.then((img) => {
    var imgElement = 
      document.createElement("img")
    imgElement.src = img.src
    document.body.appendChild(imgElement)
	})
```

`loadImagePromised()` will return a promise. Promise has a method called `then()`. We are calling `then()` and passing it a callback function. When the image has finished loading, this callback will insert the image in the body.

Promise is something we can pass around and write code around it even though we don't have the value yet.

#### Here's how we would write it with callback

```javascript
import loadImageCallbacked
from './load-image-callbacked'

let addImg = (src) => {
  let imgElement = document.createElement("img")
  imgElement.src = src
  document.body.appendChild(imgElement)
}

loadImageCallbacked('images/cat1.jpg',
  (error, img) => {
    addImg(img1.src)
  }
)

// And if we want to add more cat images
loadImageCallbacked('images/cat1.jpg', (error, img1) => {
  addImg(img1.src)  

  loadImageCallbacked('images/cat2.jpg', (error, img2) => {
    addImg(img2.src)  

    loadImageCallbacked('images/cat3.jpg', (error, img3) => {
      addImg(img3.src)  
    })
  })
})
```

It is very ugly code. And this code is not executing in parallel, because the second callback will not be called until the first image is loaded.

This is what it looks like inside `loadImageCallbacked()`

```javascript
function loadImageCallbacked(url, callback) {
  let image = new Image() // creates an image
  
  // when image is loaded, it will make a callback
  image.onload = function() {
    callback(null, image) // null means no error
  }
  
  // if there's an error, it'll execute callback with error message
  image.onerror = function() {
    let message = 'Could not load image at ' + url
    callback(new Error(msg))
  }

  image.src = url
}
export default loadImageCallbacked
```
## Promise Example
This is how a promise will look like

```javascript
import 'babelify/polyfill'

function loadImagePromised(url) {
  return new Promise((resolve, reject) => {
    let image = new Image()

    image.onload = function() {
      resolve(image)
    }

    image.onerror = function() {
      let message = 'Could not load image at ' + url
      reject(new Error(msg))
    }

    image.src = url
  })
}
export default loadImagePromised
```

```javascript
import loadImagePromised
from './load-image-promised'

let addImg = (src) => {
  let imgElement = document.createElement("img")
  imgElement.src = src
  document.body.appendChild(imgElement)
}


Promise.all([
  loadImagePromised('images/cat1.jpg'),
  loadImagePromised('images/cat2.jpg'),
  loadImagePromised('images/cat3.jpg')
]).then((images) => {
  console.log(images)
  // [img, img, img]

  images.forEach( img => addImg(img.src))
}).catch((error) => {
  // handle error
```

`Promise.all()` returns a new promise which we call `then()` on. And we are passing in a callback to `then()`, and that callback will be called with an array of the actual values that these promises returned and for each image we are adding it to the DOM.

We can use `catch()` to catch any errors that occur in `loadImagePromised`





# Async/Await
async and await allows us to pause the execution of functions, and that allows us to write asynchronous code that reads like synchronous code.

#### Some code in plain Promises

```javascript
const fetch = require('node-fetch') // a node module

function fetchAvatarUrl(userId) {
  return fetch('catapp.com/users/${userId}')
    .then(response => response.json()) // this is a promise that resolves into this json => Promise { name: 'mpg', cats: [21, 33, 45], imageUrl: 'http://images.cdn/user-123.jpg' }
    .then(data => data.imageUrl)
}

const result = fetchAvatarUrl(123)
result 

// outputs a promise that resolves to the imageUrl of the user:
// Promise http://images.cdn/user-123.jpg
```

#### Rewriting in async/await

```javascript
async function fetchAvatarUrl(userId) {
  const response = fetch('catapp.com/users/${userId}')
  // returns a promise that resolves into a response object
  // => Promise ...{response}

  // If we add keyword await
  const response = await fetch('catapp.com/users/${userId}')
  // returns the actual reponse object, NOT a promise
  // => ...{response}

  const data = response.json()
  // data is now a promise that resolves into the json data:
  // => Promise { name: 'mpg', cats: [21, 33, 45], imageUrl: 'http://images.cdn/user-123.jpg' }

  // Now if we add await
  const data = await response.json()
  // No longer a promise being returned, it's the actual data
  // => { name: 'mpg', cats: [21, 33, 45], imageUrl: 'http://images.cdn/user-123.jpg' }

  return data.imageUrl
}

const result = fetchAvatarUrl(123)
// returns a promise that resolves to fetchAvatarUrl(123)
// Promise http://images.cdn/user-123.jpg
```
## Description of `async/await`
Inside a function marked as ~~async~~, you are allowed to place the ~~await~~ keyword in front of an expression that returns a Promise. When you do, the execution of the ~~async~~ function is paused until that Promise is resolved.

#### why do we need to use `async/await`?
The idea behind `async` `await` keywords is to write asynchronous code that flows like synchronous code.

Fetch the profile images of all the cats that a user owns with Promise
```javascript
function fetchAvatarUrl(userId) {
  return fetch('catapp.com/users/${userId}')
    .then(response => response.json())
    .then(user => {
      user  // { name: 'mpg', cats: [21, 33, 45], imageUrl: 'http://images.cdn/user-123.jpg' }
      
      // Iterate over cats
      const promises = user.cats.map(catId => {
        catId // 21, 33, 45
        
        // transforms each cat into a Promise url
        return fetch('catapp.com/cats/${catId}')
          .then(response => response.json())
          .then(catData => catData.imageUrl)
          // returns [Promise {}, Promise {}, Promise {}]
      })

      return Promise.all(promises)
    })
}

const result = fetchAvatarUrl(123)
result 

// returns an array of cat's avatar
// ['http://images.cdn/cat-21.jpg', 'http://images.cdn/cat-33.jpg', 'http://images.cdn/cat-45.jpg']
```

#### Rewrite in `await/async`
```javascript
async function fetchAvatarUrl(userId) {
  const response = await fetch('catapp.com/users/${userId}')
  const user = await response.json() // { name: 'mpg', cats: [21, 33, 45], imageUrl: 'http://images.cdn/user-123.jpg' }

  const catImageUrls = []
  for (const catId of user.cats) {
    const response = await fetch('catapp.com/cats/${catId}')
    const catData = await response.json() // { {name: 'Sniffles', imageUrl: 'http://images.cdn/cat-21.jpg'}, {name: 'Snow' ...} }

    catImage.push(catData.imageUrl)
  }
  return catImageUrls
}

const result = fetchAvatarUrl(123)
result 
// returns promise of result
// Promise ['http://images.cdn/cat-21.jpg', 'http://images.cdn/cat-33.jpg', 'http://images.cdn/cat-45.jpg']
```
#### Main difference between Promises and async/await
- async/await example is less straightforward
- Promise and async/await are not logically equivalent
- async/await will load the cats in sequence (one by one)
- the Promise example loads the cats in parallel

Promise example is a lot faster. The above catapp example is to demonstrate that `await` only works for single Promises, it cannot wait for multiple Promises.


## Benefit for async/await

Here's a mix of Promise and async/await to fetch cat images in **parallel**.
```javascript
async function fetchAvatarUrl(userId) {
  const response = await fetch('catapp.com/users/${userId}')
  const user = await response.json() // { name: 'mpg', cats: [21, 33, 45], imageUrl: 'http://images.cdn/user-123.jpg' }
  
  return await Promise.all(user.cats.map(async function(catId) {
    const response = await fetch('catapp.com/cats/${catId}')
    const catData = await response.json() // { {name: 'Sniffles', imageUrl: 'http://images.cdn/cat-21.jpg'}, {name: 'Snow' ...} }

    return catData.imageUrl
  }))
}

const result = fetchAvatarUrl(123)
result 
// ['http://images.cdn/cat-21.jpg', 'http://images.cdn/cat-33.jpg', 'http://images.cdn/cat-45.jpg']
```

We passed an `async` function inside `map()`. This works because **async functions always returns a Promise**. 

`await` is mostly used when we are processing thousands of data, and we don't want to process them all at the same time using Promises.