---
title: Functions
---
<!-- MarkdownTOC -->

* [Function Overview](#function-overview)
* [Recursion](#recursion)
* [Higher order functions](#higher-order-functions)
  * [`map()` method](#map-method)
  * [`filter()` method](#filter-method)
  * [`reduce()` method](#reduce-method)
  * [Advance `reduce()`](#advance-reduce)
* [Currying](#currying)

<!-- /MarkdownTOC -->

# Function Overview
Functions are a value. 

```javascript
var triple = function(x) {
  return x* 3;
}
var waffle = triple;
waffle(30);
```


#### Structure of Function
Function have a set of ~~parameters~~ and a ~~body~~
```javascript
// structure of function
var variable = function(parameters) {
  body;
}
// function declaration
function variable(parameter) {
body;
}
variable(arguments)
```
#### Function ordering
**Function ordering does not matter**. It is ok to define a function below the code that uses it. Functions are conceptually moved to the top of their scope. This is useful as it gives us freedom to order code in a way that is meaningful.

# Recursion
Recursion
:  A function that calls itself until it doesn't.

```javascript
var countDownFrom = function(num) {
  // We need a stop condition or else this recursive function will reach call stack limit!
  if (num === 0) return;

  console.log(num)
  countDownFrom(num - 1)
}

countDownFrom(10);

// Outputs
// 10
// 9
// ...
// 1
```
It is possible for all loops to be written recursively. However, not all recursive functions can be written in a loop!

#### Practical example for recursion

```javascript
let categories = [
  { id: 'animals', 'parent': null},
  { id: 'mammals', 'parent': 'animals'},
  { id: 'cats', 'parent': 'mammals'},
  { id: 'dogs', 'parent': 'mammals'},
  { id: 'chihuahua', 'parent': 'dogs'},
  { id: 'labrador', 'parent': 'dogs'},
  { id: 'persian', 'parent': 'cats'},
  { id: 'siamese', 'parent': 'cats'},
]
// We want it to look like this:
/*
{
  animals: {
    mammals: {
      dogs: {
        chihuahua: null,
        labrador: null
      },
      cats: {
        persian: null,
        siamese: null
      }
    }
  }
}
*/

var makeTree = (categories, parent) => {
  let node = {}
  categories
    .filter(c => c.parent === parent)
    .forEach(c => 
      node[c.id] = makeTree(categories, c.id)
    )

  return node
}
console.log(makeTree(categories, null));
```


# Higher order functions
Higher-order functions
:  Functions that operate on other functions, either by taking them as arguments or by returning them.

```javascript
// Print out each item in array
function forEach(array, action) {
  for (var i = 0; i< array.length; i++)
    action(array[i]);
    //action(array[i] becomes -> console.log(array[i] )
}
forEach(["water", "tent", "cow"], console.log);
// -> water
// -> tent
// -> cow
```
## `map()` method
The `map()` method creates a new array with the results of calling a provided function on every element in this array. Think of `map()` as a "for each" loop for ~~transforming~~ values - one input value corresponds to one 'transformed' output value

```javascript
// Without map()
var numbers = [1, 2, 3, 4];
var newNumbers = [];
for (var i = 0; i < numbers.length; i++) {
  newNumbers[i] = numbers[i] * 2;
}
// -> [2, 4, 6, 8]

// Now with map()
var numbers = [1, 2, 3, 4] ;
numbers.map(function(number){
  return number * 2;
}); // -> [2, 4, 6, 8]
```

## `filter()` method
The `filter()` method creates a new array with all elements that pass the test implemented by the provided callback function. The `filter()` method will loop through each item in the array, and for each item it will pass the item as an argument into the callback function. When the item is passed into the callback function, it will expect the callback function to return either true or false. If it's true the item will get added onto the new array, if not it will be left out. 

```javascript
// Get even numbers without filter()
var numbers = [1, 2, 3, 4];
var newNumbers = [];
for (var c = 0; c < numbers.length, c++) {
  if (numbers[c] % 2 == 0) {
    newNumbers [c] = numbers [c];
  }
}
// -> [2, 4]

// With filter()  
var numbers = [1, 2, 3, 4]
var newNumbers = numbers.filter(function(number) {
  return (number % 2 == 0);
}
```

## `reduce()` method
The `reduce()` method reduces the array to a single value. Just like `map()` and `filter()`, it takes a callback function. Unlike `map()` and `filter()`, it wants an object. This object is the starting point of our `reduce()` method. The first argument in the callback function is called the accumlator (total/result). The second argument is the iterated item for each element in the array.

```javascript
var orders = [
  { amount: 250 },
  { amount: 400 },
  { amount: 100 },
  { amount: 325 },
]
// Get total amount with for loop
var totalAmount = 0
for (var i = 0, i < orders.length; i++) {
  totalAmount += orders[i].amount
}

console.log(totalAmount) // 1075

// Get total amount with reduce()
var totalAmount = orders.reduce(function(sum, order) {
  console.log("hello", sum, order)
  return sum + order.amount
}, 0) // 0 is the starting point

// This is what happens when each item gets passed into the callback function
/*
hello 0 {amount: 250}
hello 250 {amount: 400}
hello 650 {amount: 100}
hello 750 {amount: 750}
*/
```

## Advance `reduce()`
`reduce()` is a very powerful method. Here's a `data.txt` file:

```text
mark johansson waffle iron 80 2
mark johansson blender 200 1
mark johansson knife 10 4
Nikita Smith waffle iron 80 1
Nikita Smith knife 10 2
Nikita Smith pot iron 20 3

```

We want to turn the data in txt file into the following object.

```javascript
{
  'mark johansson': [
    {name: 'waffle iron', price: '80',  quantity: '2'},
    {name: 'blender',     price: '200', quantity: '1'},
    {name: 'knife',       price: '10',  quantity: '4'},
  ],
  'Nikita Smith': [
    {name: 'waffle iron', price: '80', quantity: '1'},
    {name: 'knife',       price: '10', quantity: '2'},
    {name: 'pot',         price: '20', quantity: '3'},
  ],

}
```

```javascript
import fs from 'fs'

var output = fs.readFeilSync('data.txt', 'utf8') // Get text content from txt file
  .trim()
  .split('\n') // Split each new line into an array
  .map(function(line) {
    line.split('\t')
  })
// Now we have
/*
[
  ['mark johansson', 'waffle iron', '80', '2'],
  ['mark johansson', 'blender', '200', '1'],
  ['mark johansson', 'knife', '10', '4'],
  ['mark johansson', 'waffle iron', '80', '1'],
  ['mark johansson', 'knife', '10', '2'],
  ['mark johansson', 'pot', '20', '3'],
]
*/

output = output.reduce(function(customer, line) {
  // line[0] is name of customer
  customer[line[0]] = customer[line[0]] || [] // outputs to {'mark johansson': [], 'Nikita Smith': []}

  // customer[line[0]] || [] makes sure that if the customer already exists, we won't override the value with new array.

  customer[line[0]].push({
    name: line[1],
    price: line[2],
    quantity: line[3]  
  })
  return customers
}, {}); // {} initializes new customer object
```

# Currying
Source: [Currying functional programming](https://www.youtube.com/watch?v=iZLP4qOwY8I)
Currying
:  Currying is when a function doesn't take all of its arguments at once. Instead it wants you to give it the first argument, and then the function returns another function, which we call on the second argument, which returns a new function, which we are suppose to call with the third argument and so on. Until all the arguments have been provided. The function at the end of the chain will be the value we want.

```javascript
let dragon = (name, size, element) =>
  name + ' is a ' +
  size + ' dragon that breathes ' +
  element + '!'

dragon('fluffykins', 'tiny', 'lightning')
// fluffykins is a tiny dragon that breathes lightning!

// Here is the same function in the currying version
let dragon =
  name => 
    size =>
      element =>
        name + ' is a ' +
        size + ' dragon that breathes ' +
        element + '!'

dragon('fluffykins')('tiny')('lightning')
```

Why is this useful?

```javascript
let dragons = [
  { name: 'fluffykins', element: 'lightning' },
  { name: 'noomi', element: 'lightning' },
  { name: 'karo', element: 'fire' },
  { name: 'doomer', element: 'timewarp' },
]

let hasElement =
  (element, obj) => obj.element == element

let lightningDragons =
  dragons.filter(x => hasElement('lightning', x))

console.log(lightningDragons)
/*
[
  { name: 'fluffykins', element: 'lightning' },
  { name: 'noomi', element: 'lightning' },
]
*/

// With currying

import _ from 'lodash'

let hasElement =
  _.curry((element, obj) => obj.element === element)

let lightningDragons =
  dragons.filter(hasElement('lightning'))
```