
document.addEventListener("DOMContentLoaded", function(event) {
  document.getElementById('toggle_toc').onclick = function(e) {
  	this.classList.toggle("open")
  	document.querySelectorAll(".container > ul:first-child")[0].classList.toggle("expand")
  }

  document.querySelectorAll(".container > ul:first-child")[0].onclick = function(e) {
  	this.classList.remove("expand")
  	document.getElementById('toggle_toc').classList.remove("open")
  }
});